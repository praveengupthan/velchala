<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Return Policy</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Return Policy</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body py-4 staticpage">
            <!-- container -->
            <div class="container">
                <h2 class="h3 flight">Return Policy for Services</h2>
                <p>While we strongly encourage you to go through the full Terms of Service below, here are a couple of highlights in non-lawyer-speak that you might be interested in:  We will vigorously protect your privacy and intellectual property. Your pictures, designs and personal information will always be your property only used by us to provide you this service. Your pictures and designs will never be copied or reused in a manner that violates your privacy or intellectual property.  Your use of our service has to be within the limits of the law. We can take action for anything that violates the law.</p>

                <p>Welcome to the website of Canvera ("Site"/ “website”) to avail the products and services of Canvera (collectively the "Canvera Products and /or Services") which is provided by Canvera Digital Technologies Private Limited (“Canvera” or "we" or "us" or "our").   You must be legally competent to enter into this Agreement. We do not render any advice, certifications, guarantees or warranties relating to the Canvera Products and / or Services which we make available on this website. The use of this site is governed by these Terms and Conditions ("Agreement"). When you ("You" or "Your") use this site, you acknowledge that you have read this Agreement and accept to be bound legally by the terms herein or such terms as may be modified from time to time.</p>

                <p>IF YOU DO NOT AGREE TO THIS AGREEMENT, PLEASE DO NOT USE THE CANVERA SERVICES OR THE SITE
                We reserve the right at any time to change all or any part of this Agreement; change the Canvera Products and/ or Services being provided, including by eliminating or discontinuing any content on or feature of the Canvera Service; and change any fees or charges for use of the Canvera Services. Any changes we make will be effective immediately upon notice, which we may provide by any means including without limitation, posting on the website or by electronic means such as mail sent to your email id registered with Canvera or through messages sent to your registered mobile number with Canvera. Your continued use of the Service after such notice will be deemed acceptance of such changes. You are responsible for checking this Agreement periodically for changes. Upon Our request, you agree to sign a non-electronic version of this Agreement.
                </p>

                <h2 class="h3 flight">About the Velchala</h2>
                <p>Canvera Services include without limitation creating customized photo books, prints, posters, calendars, share books, moment books, canvas, home decor, gifting where You can upload photos, create product, place order, make online payment etc.</p>

                <h2 class="h3 flight">Eligible Users</h2>
                <p>You must be above eighteen (18) years of age to utilize Canvera Services and you hereby formally affirm to have completed eighteen (18) years of age at the time use of this Site.</p>

                <h2 class="h3 flight">Registration</h2>
                <p>Utilization of Canvera Services available on Our Site requires registration. Registration shall not be available to users below eighteen (18) years of age. Each registration is for a single individual user only. In order to register, you will need an individual user name or Login ID ("Login ID") and a password.</p>

                <p class="fsbold">You agree to</p>
                <p>Provide true, accurate, current and complete data about yourself on the Site's registration form ("Registration Data");
                Promptly update the Registration Data to keep it true, accurate, current and complete. If you fail to do any of this, we will have the right to suspend or terminate your use of Canvera Service and/or terminate your account with Canvera. You will receive a password and account upon completing the registration form. You are solely responsible for maintaining the confidentiality of your password and account, and you are solely responsible for all use of your password or account, whether authorized by you or not; Not have a Login ID that is vulgar, attempts to impersonate another person or violates the law or the rights of others. We may reject any Login ID that we determine in our discretion is unacceptable; Immediately notify us of any unauthorized use of your password or account or any other breach of security;
                </p>

                <p>Ensure that you log out/exit from your account each time you complete using Canvera Services through your account with us. As a registered partner of Canvera, you will be eligible to receive communication via IVR, SMS, Call, Whatsapp and email. You can choose to opt out from receving IVR communication by calling 1-800-419-0570. You can also email us at care@canvera.com to opt out from the IVR messages.Canvera will send an opt out SMS to registered partners every 180 days to unsubscribe from the IVR service.</p>

                <p>You acknowledge and agree that access and use of password protected and/or secure areas of Canvera Service are restricted to users who have been given a valid password by Our Site. You agree not to access these areas of the Service without appropriate authorization and acknowledge that in the event you attempt to access/use such restricted areas of the Canvera Services, you may be subject to prosecution.</p>
            </div>
            <!--/ container -->
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?> 
    </body>
</html>