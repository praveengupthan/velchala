<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

    <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
     <!-- header sub page -->
     <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Interviews</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>   
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Blog</a></li>                   
                        <li class="breadcrumb-item active" aria-current="page"><span>Interviews</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row py-3">
            <!-- col -->
            <div class=" col-sm-6 col-lg-4 wow animate__animated animate__fadeInUp">
                <div class="card ">
                    <iframe width="100%" height="225" src="https://www.youtube.com/embed/sXOQ0l3aRzQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">                       
                        <p class="card-text pb-3">Kondal Rao Interview on his Personal Life</p>
                    </div>
                </div>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-sm-6 col-lg-4 wow animate__animated animate__fadeInDown">
                <div class="card ">
                    <iframe width="100%" height="225" src="https://www.youtube.com/embed/xRr-Nn73UHs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">                       
                        <p class="card-text pb-3">Dr VELCHALA KONDAL RAO GARU BOOK RELEASE....</p>
                    </div>
                </div>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-sm-6 col-lg-4 wow animate__animated animate__fadeInUp">
                <div class="card">
                    <iframe width="100%" height="225" src="https://www.youtube.com/embed/dWFvZl3D3us" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">                       
                        <p class="card-text pb-3">Dr. Velchala Kondal Rao Speech on Viswanadha Sahitya Peetham ....</p>
                    </div>
                </div>
            </div>
            <!--/ col -->

        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

    <?php include 'includes/footer.php'?>
   <?php include 'includes/scripts.php' ?> 
   
    </body>
</html>