
//great people slider 
var swiper = new Swiper('.greatpeople-in', {
  slidesPerView: 1,
  spaceBetween: 5,
 autoplay: {
    delay: 5000,
    disableOnInteraction: true,
  },
  // init: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    320: {
      slidesPerView: 3,
      spaceBetween: 10,
    },
    640: {
      slidesPerView: 5,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 6,
      spaceBetween: 40,
    },
    1024: {
      slidesPerView: 13,
      spaceBetween: 10,
    },
    1500: {
      slidesPerView: 16,
      spaceBetween: 10,
    },
  }
});



//add class to header on scroll
$(window).scroll(function(){
    if($(this).scrollTop() > 20){
        $('.fixed-top').addClass('fixed-theme', 1000);
        
    }else{
        $('.fixed-top').removeClass('fixed-theme', 1000);
    }
});



  //image gallery
  baguetteBox.run('.grid-gallery', { animation: 'slideIn'});

  //poems
  var swiper = new Swiper('.poems-container', {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

	  //swiper publications
    var swiper = new Swiper('.home-publications', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 1,
        depth: 0,
        modifier: 0.8,
        slideShadows : true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });


    

  //tooltip
  $(function () {
      $('[data-toggle="tooltip"]').tooltip()
  })


//header search
  $(document).ready(function(){    
    $('#searchIcon').click(function(){
       $('.search-visible').addClass('search-block');
       $('.bsnav').hide();
    });   
    $('#closeSearchIcon') .click(function(){
       $('.search-visible').removeClass('search-block');
       $('.bsnav').show();
    });
  });


  //velchala single page
  let mainNavLinks = document.querySelectorAll("nav ul li a");
  let mainSections = document.querySelectorAll(".singleBody section");

  let lastId;
  let cur = [];


  window.addEventListener("scroll", event => {
    let fromTop = window.scrollY;

    mainNavLinks.forEach(link => {
      let section = document.querySelector(link.hash);

      if (
        section.offsetTop <= fromTop &&
        section.offsetTop + section.offsetHeight > fromTop
      ) {
        link.classList.add("current");
      } else {
        link.classList.remove("current");
      }
    });
  });



  $(document).ready(function() {
    //Horizontal Tab
    $('.parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
    // Child Tab
    $('#ChildVerticalTab_1').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true,
        tabidentify: 'ver_1', // The tab groups identifier
        activetab_bg: '#fff', // background color for active tabs in this group
        inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
        active_border_color: '#c1c1c1', // border color for active tabs heads in this group
        active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
    });
    //Vertical Tab
    $('#parentVerticalTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
});


//star rating 
(function ($) {
  $.fn.addRating = function (options) {
      var obj = this;
      var settings = $.extend({
          max: 5,
          half: true,
          fieldName: 'rating',
          fieldId: 'rating',
          icon: 'star',
          selectedRatings:0
      }, options);
      this.settings = settings;

      // create the stars
      for (var i = 1; i <= settings.max; i++) {
          var star = $('<i/>').addClass('material-icons').html(this.settings.icon + '_border').data('rating', i).appendTo(this).click(
              function () {
                  obj.setRating($(this).data('rating'));
              }
          ).hover(
              function (e) {
                  obj.showRating($(this).data('rating'), false);
              }, function () {
                  obj.showRating(0, false);
              }
          );

      }
      $(this).append('<input type="hidden" name="' + settings.fieldName + '" id="' + settings.fieldId + '" value="' + settings.selectedRatings + '" />');

      obj.showRating(settings.selectedRatings, true);
  };

  $.fn.setRating = function (numRating) {
      var obj = this;
      $('#' + obj.settings.fieldId).val(numRating);
      obj.showRating(numRating, true);
  };

  $.fn.showRating = function (numRating, force) {
      var obj = this;
      if ($('#' + obj.settings.fieldId).val() == '' || force) {
          $(obj).find('i').each(function () {
              var icon = obj.settings.icon + '_border';
              $(this).removeClass('selected');

              if ($(this).data('rating') <= numRating) {
                  icon = obj.settings.icon;
                  $(this).addClass('selected');
              }
              $(this).html(icon);
          })
      }
  }

}(jQuery));

$(document).ready(function(){
  $('.rating').addRating();
})



//remove address
$('.delete-addressicon').click(function(){
  $(this).parents('.address-block').hide();
});

//delete wishlist item
$('.deleteWishlistIcon').click(function(){
  $(this).parents('.wishlistItem').hide();
});

//delete checkout item 
$('.removeitem_eheckoutbtn').click(function(){
  $(this).parents('.checkout-row').hide();
});


//accordion
 // Hiding the panel content. If JS is inactive, content will be displayed
 $( '.panel-content' ).hide();

 // Preparing the DOM
 
 // -- Update the markup of accordion container 
 $( '.accordion' ).attr({
   role: 'tablist',
   multiselectable: 'true'
  });

 // -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
 $( '.panel-content' ).attr( 'id', function( IDcount ) { 
   return 'panel-' + IDcount; 
 });
 $( '.panel-content' ).attr( 'aria-labelledby', function( IDcount ) { 
   return 'control-panel-' + IDcount; 
 });
 $( '.panel-content' ).attr( 'aria-hidden' , 'true' );
 // ---- Only for accordion, add role tabpanel
 $( '.accordion .panel-content' ).attr( 'role' , 'tabpanel' );
 
 // -- Wrapping panel title content with a <a href="">
 $( '.panel-title' ).each(function(i){
   
   // ---- Need to identify the target, easy it's the immediate brother
   $target = $(this).next( '.panel-content' )[0].id;
   
   // ---- Creating the link with aria and link it to the panel content
   $link = $( '<a>', {
     'href': '#' + $target,
     'aria-expanded': 'false',
     'aria-controls': $target,
     'id' : 'control-' + $target
   });
   
   // ---- Output the link
   $(this).wrapInner($link);  
   
 });

 // Optional : include an icon. Better in JS because without JS it have non-sense.
 $( '.panel-title a' ).append('<span class="icon">+</span>');

 // Now we can play with it
 $( '.panel-title a' ).click(function() {
   
   if ($(this).attr( 'aria-expanded' ) == 'false'){ //If aria expanded is false then it's not opened and we want it opened !
     
     // -- Only for accordion effect (2 options) : comment or uncomment the one you want
     
     // ---- Option 1 : close only opened panel in the same accordion
     //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
     $(this).parents( '.accordion' ).find( '[aria-expanded=true]' ).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');

     // Option 2 : close all opened panels in all accordion container
     //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);
     
     // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
     $(this).attr( 'aria-expanded' , true ).addClass( 'active' ).parent().next( '.panel-content' ).slideDown(200).attr( 'aria-hidden' , 'false');

   } else { // The current panel is opened and we want to close it

     $(this).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');;

   }
   // No Boing Boing
   return false;
 });


 //onload window popup video


 //jayanthi detail 
 var swiper = new Swiper('.jayanthi-detail', {
  slidesPerView: 20,
  spaceBetween: 10,
  loop: false,
  autoplay:false,
 
  // init: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    320: {
      slidesPerView: 2,
      spaceBetween: 60,
    },
    640: {
      slidesPerView: 6,
      spaceBetween: 60,
    },
    768: {
      slidesPerView: 8,
      spaceBetween: 40,
    },
    1024: {
      slidesPerView: 12,
      spaceBetween: 10,
    },
  }
});



    //home page text animation in slider
    
    $("#typed2").typing({
      strings: ['Writer', 'Journalist', 'Educationist', 'Administrator', 'Poet', 'Translate','Global Trotter'],
      eraseDelay: 20,
      typeDelay: 100,
      stringStartDelay: 1000,
      color: 'white',
      typingColor: 'black',
      typingOpacity: '0.1',
      loopCount: 20,
      cursorBlink: true,
      cursorChar: '<small>_</small>',
      fade: true,
      onTyping: function () {
          console.log('onTyping');
      },
      onFinishedTyping: function () {
          console.log('onFinishedTyping');
      },
      onErasing: function () {
          console.log('onErasing');
      },
      onFinishedErasing: function () {
          console.log('onFinishedErasing');
      },
      onAllTypingCompleted: function () {
          console.log('onAllTypingCompleted');
      },
      onFinishedFadeErasing: function () {
          console.log('onFinishedFadeErasing');
      }
  });

  $("#typed3").typing({
    strings: ['రచయిత', 'జర్నలిస్ట్', 'విద్యావేత్త', 'నిర్వాహకుడు', 'కవి', 'అనువాదకుడు','గ్లోబల్  ట్రోటర్'],
    eraseDelay: 20,
    typeDelay: 100,
    stringStartDelay: 1000,
    color: 'white',
    typingColor: 'black',
    typingOpacity: '0.1',
    loopCount: 20,
    cursorBlink: true,
    cursorChar: '<small>_</small>',
    fade: true,
    onTyping: function () {
        console.log('onTyping');
    },
    onFinishedTyping: function () {
        console.log('onFinishedTyping');
    },
    onErasing: function () {
        console.log('onErasing');
    },
    onFinishedErasing: function () {
        console.log('onFinishedErasing');
    },
    onAllTypingCompleted: function () {
        console.log('onAllTypingCompleted');
    },
    onFinishedFadeErasing: function () {
        console.log('onFinishedFadeErasing');
    }
});




  
//on click move to browser top
$(document).ready(function(){

  $(".move-top-video").click(function() {
    $('html,body').animate({
        scrollTop: $(".vspsection").offset().top},
        'slow');               
  });
  
  $(window).scroll(function(){
      if($(this).scrollTop() > 50){
          $('#movetop').fadeIn()
      }else{
          $('#movetop').fadeOut();
      }
  });

  //velchala page 
  $(window).scroll(function(){
    if($(this).scrollTop()>50){
      $('.velchala-page header').fadeOut();
    } else{
      $('.velchala-page header').fadeIn();
    }
  });

  //click event to scroll to top
  $('#movetop').click(function(){
      $('html, body').animate({scrollTop:0}, 200);
  });
  
});


//video backgrond
$('.video-background').youtubeBackground({
  videoId: 'WRBTxGCYxHo',
  // backgroundColor: '#212121',
  // backgroundImage: 'https://i.ytimg.com/vi/ITpIv6Efz8Y/maxresdefault.jpg', // For mobile devices
  opacity: 0.6
});


// document.onmousedown = disableRightclick;
// var message = "Right click not allowed !!";
// function disableRightclick(evt){
//     if(evt.button == 2){
//         alert(message);
//         return false;    
// }
// }

   var swiper = new Swiper('.velchalaBan', {
      spaceBetween: 30,
      effect: 'fade',
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      autoplay: {
        delay: 5000,
        disableOnInteraction: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });