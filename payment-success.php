<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">

        <div class="container payment-success">
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-lg-6 text-center">
                    <img src="img/payment-success-icon.svg" class="success-icon">
                    <p class="pt-3">Thank you! Your Payment of Rs: 3,000 has been received</p>
                    <p class="d-flex justify-content-between pt-3">
                        <span>Order ID: <span class="flight">IC-12334567</span></span>
                        <span>Transaction ID: <span class="flight">123456</span></span>
                    </p>
                    <h5 class="h6 border-top py-4 mt-3"> Click on the following Button, You Can Track the Status, Payment Details of Product</h5>

                    <p class="d-flex justify-content-center flex-wrap">
                        <a href="publications.php" class="orange-btn">Continue Shopping</a>
                        <a href="user-orders.php" class="orange-btn-border ml-3 mt-3 mt-sm-0">Get Invoice</a>
                    </p>
                </div>
                <!--/ col -->
            </div>
        </div>
      

    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?> 

    </body>
</html>