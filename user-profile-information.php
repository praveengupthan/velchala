<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">
   <?php include 'includes/header-postlogin.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Profile Information</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="user-profile-information.php">Praveen Guptha</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>User Profile Information</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row py-3 userprofile-row">
                <!-- left col -->
                <div class="col-md-4 col-sm-4 wow animate__animated animate__fadeInDown">
                    <?php include 'includes/usrleftnav.php' ?>
                </div>
                <!--/ left col -->
                <!-- right col -->
                <div class="col-lg-8 col-sm-8 wow animate__animated animate__fadeInUp">
                    <!-- right profile detail -->
                    <div class="user-profile-rt">

                        <!-- row -->
                        <div class="row justify-content-center">
                            <!-- col -->
                            <div class="col-lg-6">
                                <div class="figure-user mx-auto position-relative">
                                    <img src="img/users-thumb/thumb06.jpg" alt="">
                                    <a data-toggle="modal" data-target="#update-profile-picture" class="edit-pic" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Edit Your Profile Picture">
                                        <span class="icon-edit icomoon"></span>
                                    </a>
                                </div>

                                <!-- form -->
                                <form class="form py-4">
                                    <!-- form group -->
                                    <div class="form-group">
                                        <label for="userName">Write Your Name</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="userName" aria-describedby="emailHelp" placeholder="Enter email" value="Praveen Guptha Nandipati">
                                        </div>
                                    </div>
                                    <!-- /form group -->

                                    <!-- form group -->
                                    <div class="form-group">
                                        <label for="userName">Gender Male / Female</label>
                                        <div class="d-flex">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="exampleRadios" id="male" value="option1" checked>
                                                <label class="form-check-label" for="male">
                                                    Male
                                                </label>
                                            </div>
                                            <div class="form-check ml-4 form-check-inline">
                                                <input class="form-check-input" type="radio" name="exampleRadios" id="female" value="option2">
                                                <label class="form-check-label" for="female">
                                                Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /form group -->

                                     <!-- form group -->
                                     <div class="form-group">
                                        <label for="mNumber">Mobile Number</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="mNumber" aria-describedby="emailHelp" placeholder="Enter Phone Number" value="91 9642123254">
                                        </div>
                                    </div>
                                    <!-- /form group -->

                                     <!-- form group -->
                                     <div class="form-group">
                                        <label for="email">Mobile Number</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter Email Address" value="praveennandipati@gmail.com">
                                        </div>
                                    </div>
                                    <!-- /form group -->

                                      <!-- form group -->
                                      <div class="form-group">
                                       <button class="btn  orange-btn w-100">Submit</button>
                                    </div>
                                    <!-- /form group -->
                                   
                                </form>
                                <!--/ form -->
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                    </div>
                    <!--/ right profile detail -->
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
       </div>
       <!-- /container -->   
                

       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
   <?php include 'includes/scripts.php' ?>    

   <!--image upload popup -->
   <!-- Modal -->
    <div class="modal fade" id="update-profile-picture" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Change Your Profile Picture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <!-- file input -->
            <form>
                    <input type="file" accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple>
                </form>
            <!--/ file input -->
            </div>
            <div class="modal-footer">       
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $('input[type="file"]').imageuploadify();
            })
        </script>
    </div>
    <!--/image upload popup -->
    </body>
</html>