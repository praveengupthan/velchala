<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">
   <?php include 'includes/header-postlogin.php' ?>

     <!-- alert wishlist-->
     <div id="alertAddrtoCart" class="alert alert-success alert-dismissible" role="alert">
        <strong><span class="icon-check-circle"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your Cart List.  <a href="javascript:void(0)" class="d-block"><strong>View Cart Items</strong></a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <!--/ alert wishlist -->
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Wishlist Items</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="user-profile-information.php">Praveen Guptha</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Wishlist Items</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row py-3 userprofile-row">
                <!-- left col -->
                <div class="col-md-4 col-sm-4">
                    <?php include 'includes/usrleftnav.php' ?>
                </div>
                <!--/ left col -->
                <!-- right col -->
                <div class="col-md-8 col-sm-8">
                    <!-- right profile detail -->
                    <div class="includes/user-profile-rt">

                    <!-- orders list item -->
                    <div class="myorder-list-item wishlistItem py-3 mb-0">                     

                        <!-- secondary details row -->
                        <div class="row wow animate__animated animate__fadeInDown">
                            <!-- col -->
                            <div class="col-md-2">
                                <a href="publication-detail.php">
                                    <img src="img/coverpages/cover01.jpg" alt="" class="img-fluid">
                                </a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-10">
                                <p class="fsbold">
                                    <a class="fblack" href="publication-detail.php">Viswanatha Sahithyam Telugu Book</a>
                                </p>
                                <p class="text-success">Instock</p>
                                <p class="price">
                                    <span class="offer">250</span>                                    
                                </p>
                                <p class="text-right">
                                    <a href="javascript:void(0)" class="addtoCart-icon fblack"><span class="icon-shopping-cart icomoon mr-1"></span>Add to Cart</a>
                                    <a href="javascript:void(0)" class="pl-3 deleteWishlistIcon fblack"><span class="icon-trash-o icomoon mr-1"></span>Delete</a>
                                </p>                                
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ secondary details row -->                        
                    </div>
                    <!--/ orders list item -->

                       <!-- orders list item -->
                       <div class="myorder-list-item wishlistItem py-3 mb-0">                     

                        <!-- secondary details row -->
                        <div class="row wow animate__animated animate__fadeInDown">
                            <!-- col -->
                            <div class="col-md-2">
                                <a href="publication-detail.php">
                                    <img src="img/coverpages/cover02.jpg" alt="" class="img-fluid">
                                </a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-10">
                                <p class="fsbold">
                                    <a  class="fblack" href="publication-detail.php">Viswanatha Sahithyam Telugu Book</a>
                                </p>
                                <p class="text-success">Instock</p>
                                <p class="price">
                                    <span class="offer">250</span>                                    
                                </p>
                                <p class="text-right">
                                    <a href="javascript:void(0)" class="addtoCart-icon fblack"><span class="icon-shopping-cart icomoon mr-1"></span>Add to Cart</a>
                                    <a href="javascript:void(0)" class="pl-3 deleteWishlistIcon fblack"><span class="icon-trash-o icomoon mr-1"></span>Delete</a>
                                </p>                                
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ secondary details row -->                        
                        </div>
                        <!--/ orders list item -->

                        <!-- orders list item -->
                        <div class="myorder-list-item wishlistItem py-3 mb-0">                     

                        <!-- secondary details row -->
                        <div class="row wow animate__animated animate__fadeInDown">
                            <!-- col -->
                            <div class="col-md-2">
                                <a href="publication-detail.php">
                                    <img src="img/coverpages/cover03.jpg" alt="" class="img-fluid">
                                </a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-10">
                                <p class="fsbold">
                                    <a  class="fblack" href="publication-detail.php">Viswanatha Sahithyam Telugu Book</a>
                                </p>
                                <p class="text-success">Instock</p>
                                <p class="price">
                                    <span class="offer">250</span>                                    
                                </p>
                                <p class="text-right">
                                    <a href="javascript:void(0)" class="addtoCart-icon fblack"><span class="icon-shopping-cart icomoon mr-1"></span>Add to Cart</a>
                                    <a href="javascript:void(0)" class="pl-3 deleteWishlistIcon fblack"><span class="icon-trash-o icomoon mr-1"></span>Delete</a>
                                </p>                                
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ secondary details row -->                        
                        </div>
                        <!--/ orders list item -->

                    </div>
                    <!--/ right profile detail -->
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
       </div>
       <!-- /container -->   
                

       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
   <?php include 'includes/scripts.php' ?>  

   <script>
    $(document).ready(function(){        
        $('.addtoCart-icon').click(function(){
            $('#alertAddrtoCart').show();
        }) 
    });
   </script>    
   
    </body>
</html>