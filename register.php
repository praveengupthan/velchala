<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">
    <div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">
                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="index.php" class="brand-login">
                                <img src="img/logo.svg" alt="">
                            </a>
                            <h1 class="text-center flight pb-0">Register with us</h1>                            
                        </div>
                        <!-- form -->
                        <form class="form py-3">
                            <div class="form-group">
                                <label for="userNameInput">Enter your Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="userNameInput" placeholder="Write Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userNameInput">Email</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="userNameInput" placeholder="Write Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userNameInput">Mobile Number</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="userNameInput" placeholder="Mobile Number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userNameInput">Create Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" id="userNameInput" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userNameInput">Confirm Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" id="userNameInput" placeholder="Confirm Password">
                                </div>
                            </div>                            
                            <input type="submit" class="btn orange-btn w-100 mt-2" value="Signup">
                            <p class="text-center">
                                Already have an account? <a class="forange" href="login.php">Sigin</a>
                            </p>
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ login section -->
                </div>
                <!--/ col -->                 
            </div>
            <!--/row -->
        </div>
        <!--/ container fluid -->
    </div>
   
    <?php include 'includes/scripts.php' ?>    
    
    </body>
</html>