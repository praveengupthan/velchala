<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
    <?php include 'includes/arrayObjects.php'?>
</head>
<body class="animsition">

 <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Album Name will be here</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>   
                        <li class="breadcrumb-item"><a href="photo-albums.php">Photo Album</a></li>                   
                        <li class="breadcrumb-item active" aria-current="page"><span>Album Name will be here</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
           <!-- container -->
           <div class="container gallery-block grid-gallery">
                <!-- row -->
                <div class="row">
                    <!-- item -->
                    <?php 
                    for($i=0;$i<count($galleryPhotoDetail);$i++) {?>
                    <div class="col-6 col-sm-6 col-md-3 item wow animate__animated animate__fadeInUp">
                        <a class="lightbox" href="img/albums/<?php echo $galleryPhotoDetail[$i][0]?>.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/albums/<?php echo $galleryPhotoDetail[$i][0]?>.jpg">
                        </a>
                    </div>
                    <?php } ?>
                    <!-- item -->                   
               </div>
               <!--/ row -->
           </div>
           <!--/ container -->
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?> 
    </body>
</html>