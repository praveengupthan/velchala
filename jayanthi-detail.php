<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition"> 
    <!--main-->   
    <main>

     <!-- Swiper -->
    <div class="swiper-container jayanthi-detail">
        <div class="swiper-wrapper" mySwiper.autoplay.stop();>
            <div class="swiper-slide"><span class="pagenumber">1</span><img src="img/coverjayanthi/1.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">2</span><img src="img/coverjayanthi/2.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">3</span><img src="img/coverjayanthi/3.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">4</span><img src="img/coverjayanthi/4.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">5</span><img src="img/coverjayanthi/5.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">6</span><img src="img/coverjayanthi/6.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">7</span><img src="img/coverjayanthi/7.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">8</span><img src="img/coverjayanthi/8.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">9</span><img src="img/coverjayanthi/9.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">10</span><img src="img/coverjayanthi/10.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">11</span><img src="img/coverjayanthi/11.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">12</span><img src="img/coverjayanthi/12.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">13</span><img src="img/coverjayanthi/1.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">14</span><img src="img/coverjayanthi/2.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">15</span><img src="img/coverjayanthi/3.jpg" class="img-fluid"></div>
            <div class="swiper-slide"><span class="pagenumber">16</span><img src="img/coverjayanthi/4.jpg" class="img-fluid"></div>
        </div>       
        <!-- <div class="swiper-pagination"></div> -->
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
    <!--/ swiper -->

    <div class="view-jayanthi">
        <img src="img/coverjayanthi/1.jpg" alt="" class="img-fluid w-100">
    </div>

    </main> 
    <!--/ main-->  
    <?php include 'includes/scripts.php' ?> 
    </body>
</html>