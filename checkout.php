<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Checkout</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Checkout</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body py-4">

            <!--container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-md-8 wow animate__animated animate__fadeInUp">
                    <!-- stepper -->
                    <div class="stepper">
                     <!--multisteps-form-->
                    <div class="multisteps-form">
                        <!--progress bar-->
                        <div class="row">
                        <div class="col-12 mb-4">
                            <div class="multisteps-form__progress">
                                <button class="multisteps-form__progress-btn js-active" type="button" title="User Info">Delivery Address</button>                               
                                <button class="multisteps-form__progress-btn" type="button" title="Comments">Order Summary </button>
                            </div>
                        </div>
                        </div>
                        <!--form panels-->
                        <!-- row -->
                        <div class="row">
                        <!-- col 12-->
                        <div class="col-12">
                            <form class="multisteps-form__form">
                            <!--Delivery Address-->
                            <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn">
                                
                                <div class="multisteps-form__content">
                                <h3 class="multisteps-form__title">Select Delivery Address</h3>

                                    <!-- address item -->
                                    <div class="address-item">
                                       <!-- row -->
                                       <div class="row">
                                           <!-- col -->
                                           <div class="col-md-1 col-12 pr-0">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>    
                                                </div>
                                            </div>
                                           <!--/ col -->
                                            <!-- col -->
                                            <div class="col-md-8 col-12 pl-0">
                                                <p class="fsbold">Praveen Guptha Nandipati</p>
                                                <p>Plot No:91, 4-32-51/51/1, Kamala Prasanna Nagar, Kukatpally, Hyderabad, Telangana, </p>
                                                <a href="javascript:void(0)" class="btn-deliver blue-btn">Deliver Here</a>
                                            </div>
                                           <!--/ col -->
                                            <!-- col -->
                                            <div class="col-md-3 text-sm-center text-right">
                                                <a href="javascript:void(0)" class="forange editicon">Edit</a>
                                            </div>
                                           <!--/ col -->
                                       </div>
                                       <!--/ row --> 
                                    </div>
                                    <!--/ address item-->

                                    
                                    <!-- address item -->
                                    <div class="address-item">
                                       <!-- row -->
                                       <div class="row">
                                           <!-- col -->
                                           <div class="col-md-1 col-12 pr-0">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1">    
                                                </div>
                                            </div>
                                           <!--/ col -->
                                            <!-- col -->
                                            <div class="col-md-8 col-12 pl-0">
                                                <p class="fsbold">Praveen Kumar (Home Address)</p>
                                                <p>Plot No:91, 4-32-51/51/1, Kamala Prasanna Nagar, Kukatpally, Hyderabad, Telangana, Allwyn Colony Phase 1, Road No:9, Hydeabad - 500072.</p>
                                                <a href="javascript:void(0)" class="btn-deliver blue-btn">Deliver Here</a>
                                            </div>
                                           <!--/ col -->
                                            <!-- col -->
                                            <div class="col-md-3 text-sm-center text-right">
                                                <a href="javascript:void(0)" class="forange editicon">Edit</a>
                                            </div>
                                           <!--/ col -->
                                       </div>
                                       <!--/ row --> 
                                    </div>
                                    <!--/ address item-->

                                    
                                    <!-- address item -->
                                    <div class="address-item">
                                       <!-- row -->
                                       <div class="row">
                                           <!-- col -->
                                           <div class="col-md-1 col-12 pr-0">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>    
                                                </div>
                                            </div>
                                           <!--/ col -->
                                            <!-- col -->
                                            <div class="col-md-8 col-12 pl-0">
                                                <p class="fsbold">Praveen Nandipati</p>
                                                <p>Plot No:91, 4-32-51/51/1, Kamala Puri Nagar, Srinagar Colony, Hyderabad, Telangana, Hydeabad - 500072.</p>
                                                <a href="javascript:void(0)" class="btn-deliver blue-btn">Deliver Here</a>
                                            </div>
                                           <!--/ col -->
                                            <!-- col -->
                                            <div class="col-md-3 text-sm-center text-right">
                                                <a href="javascript:void(0)" class="forange editicon">Edit</a>
                                            </div>
                                           <!--/ col -->
                                       </div>
                                       <!--/ row --> 
                                    </div>
                                    <!--/ address item-->

                                    <p class="text-center">
                                        <a data-toggle="modal" data-target="#newAddress" class="fblue" href="javascript:void(0)">+ Add New Address</a>
                                    </p>
                             
                           
                                    <div class="button-row d-flex mt-4">
                                        <button class="btn orange-btn ml-auto js-btn-next" type="button" title="Next">Order Summary</button>
                                    </div>
                                </div>
                            </div>
                            <!--Delivery Address-->

                            <!--Order Summary--> 
                            <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                <h3 class="multisteps-form__title">Order Summary</h3>
                                <div class="multisteps-form__content">
                                 <p class="text-right fblue"><small>Deliver by 24 August 2020</small></p>
                                 <p class="text-center">No Products Available in Checkout</p>
                                <!-- order summary -->
                                <div class="row checkout-row">
                                    <!-- col -->
                                    <div class="col-md-2">
                                        <img src="img/coverpages/cover01.jpg" alt="" class="img-fluid">
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-8">
                                        <h6 class="h6 fsbold pb-2">Jayanthi Telugu 325 Pages Book</h6>
                                        <p class="price-single forange pb-0">258</p>
                                        <input type="number" value="1" class="form-control text-center" style="width:50px;"> 
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-2">
                                        <p>
                                            <a class="pt-3 removeitem_eheckoutbtn" href="javascript:void(0)">Remove</a>
                                        </p>
                                    </div>
                                    <!--/col -->
                                </div>                               

                                <!--/ order summary -->

                                  <!-- order summary -->
                                  <div class="row checkout-row">
                                    <!-- col -->
                                    <div class="col-md-2">
                                        <img src="img/coverpages/cover02.jpg" alt="" class="img-fluid">
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-8">
                                        <h6 class="h6 fsbold pb-2">Jayanthi Telugu 325 Pages Book</h6>
                                        <p class="price-single forange pb-0">258</p>
                                        <input type="number" value="1" class="form-control text-center" style="width:50px;"> 
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-2">
                                        <p>
                                            <a class="pt-3 removeitem_eheckoutbtn" href="javascript:void(0)">Remove</a>
                                        </p>
                                    </div>
                                    <!--/col -->
                                </div>                               

                                <!--/ order summary -->

                                  <!-- order summary -->
                                  <div class="row checkout-row">
                                    <!-- col -->
                                    <div class="col-md-2">
                                        <img src="img/coverpages/cover03.jpg" alt="" class="img-fluid">
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-8">
                                        <h6 class="h6 fsbold pb-2">Jayanthi Telugu 325 Pages Book</h6>
                                        <p class="price-single forange pb-0">258</p>
                                        <input type="number" value="1" class="form-control text-center" style="width:50px;"> 
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-2">
                                        <p>
                                            <a class="pt-3 removeitem_eheckoutbtn" href="javascript:void(0)">Remove</a>
                                        </p>
                                    </div>
                                    <!--/col -->
                                </div>                               

                                <!--/ order summary -->
                               
                                    <div class="button-row d-flex mt-4">
                                        <button class="btn orange-btn-border js-btn-prev" type="button" title="Prev">Change Address</button>
                                        <!-- <button onclick="window.location.href='payment-success.php'" class="btn orange-btn ml-auto" type="button" title="Send">Pay Now</button> -->
                                    </div>
                                </div>
                            </div>
                            <!--/ order summary -->
                            </form>
                        </div>
                        <!--/ col 12-->
                        </div>
                        <!--/ row -->
                    </div>

                    </div>
                    <!--/ stepper -->
                      

                    </div>
                    <!--/ left col -->

                     <!-- left col -->
                     <div class="col-md-4 wow animate__animated animate__fadeInDown">
                         <!-- card -->
                         <div class="card cartcard">
                             <div class="card-header bggray">
                                 <h4 class="h6 fsbold">Order Summary</h4>
                             </div>
                             <div class="card-body">
                                 <p class="h6 d-flex justify-content-between  pb-4">
                                    <span>ITEMS 3</span>
                                    <span class="price-single">750</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between  pb-4 border-bottom">
                                    <span>Shipping Charges</span>
                                    <span class="price-single">150</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between fsbold py-4">
                                    <span>Total Cost</span>
                                    <span class="price-single fblue">900</span>
                                 </p>                           
                             </div>
                         </div>
                         <!--/ card -->

                         <!-- card -->
                         <div class="card cartcard mt-3">
                             <div class="card-header bggray">
                                 <h4 class="h6 fsbold">Billing Address</h4>
                             </div>
                             <!-- card body -->
                             <div class="card-body">
                                <form>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Same as Shipping Address
                                        </label>
                                    </div>
                                    <!-- show billing address form -->
                                    <div class="showbilling-address">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                                            <label class="form-check-label" for="exampleRadios2">
                                                User Different Billing Address
                                            </label>

                                            <!-- form group -->
                                            <div class="form-group">
                                                <label>Name</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Write Name">
                                                </div>
                                            </div>
                                            <!--/ form group -->
                                             <!-- form group -->
                                             <div class="form-group">
                                                <label>Mobile Number</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Mobile Number">
                                                </div>
                                            </div>
                                            <!--/ form group -->
                                            <!-- form group -->
                                            <div class="form-group">
                                                <label>Select Country</label>
                                                <div class="input-group">
                                                    <select class="form-control" disabled>
                                                        <option>India</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--/ form group -->
                                            <!-- form group -->
                                            <div class="form-group">
                                                <label>Select State</label>
                                                <div class="input-group">
                                                    <select class="form-control">
                                                        <option>Select State</option>
                                                        <option>Andhra Pradesh</option>
                                                        <option>Telangana</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--/ form group -->

                                             <!-- form group -->
                                             <div class="form-group">
                                                <label>Complete Address</label>
                                                <div class="input-group">
                                                   <textarea class="form-control" style="height:75px;" placeholder="Write Complete Address"></textarea>
                                                </div>
                                            </div>
                                            <!--/ form group -->

                                             <!-- form group -->
                                             <div class="form-group">
                                                <label>Pincode</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control text-left" placeholder=" Area Pincode">
                                                </div>                                                
                                            </div>
                                            <!--/ form group -->

                                            
                                             <!-- form group -->
                                             <div class="form-group">
                                                <label>Landmark</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder=" Area Land Mark">
                                                </div>
                                                <small>Entering Landmark will help us identifying your location faster</small>
                                                
                                            </div>
                                            <!--/ form group -->
                                            <button onclick="window.location.href='payment-success.php'" class="btn orange-btn ml-auto w-100" type="button" title="Send">Pay &nbsp;  <span class="price-single">900</span> </button>

                                        </div>
                                    </div>
                                    <!--/ show billing address form -->
                                </form>
                             </div>
                             <!--/ card body -->
                         </div>
                         <!--/ card -->


                     </div>
                    <!--/ left col -->


                </div>
                <!--/ row -->
            </div>
            <!--/ container -->           
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?> 

    <!-- Add New Address -->
    <div class="modal fade" id="newAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Add New Address</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">

        <!--form-->
        <form class="form">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="firstName">First Name</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="firstName" aria-describedby="firstName" placeholder="First Name">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="secondName">Second Name</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="secondName" aria-describedby="firstName" placeholder="Second Name">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                
                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="address01">Address Line 01</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="address01" aria-describedby="address01" placeholder="Address Line 01">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="address02">Address Line 02</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="address02" aria-describedby="address02" placeholder="Address Line 02">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="pinNumber">Pin Number</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="pinNumber" aria-describedby="pinNumber" placeholder="Write Pin Number">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="landmark">Land Mark</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="landmark" aria-describedby="landmark" placeholder="Ex:Beside Hospital">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="city">City</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="city" aria-describedby="city" placeholder="Ex:Hyderabad">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                
                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="State">State</label>
                            <div class="input-group">
                                <select id="State" class="form-control">
                                    <option selected>Choose...</option>
                                    <option>Telangana</option>
                                    <option>Andhra Pradesh</option>
                                    <option>Tamil Nadu</option>
                                    <option>Karnataka</option>
                                    <option>Kerala</option>
                                    <option>Madhya Pradesh</option>
                                </select>
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                
                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="mNumber">Mobile Number</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="mNumber" aria-describedby="mNumber" placeholder="Mobile Number">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="email">Email</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="email" aria-describedby="email" placeholder="Enter Valid Email Address">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

            </div>
            <!--/ row -->
        </form>
        <!--/ form -->
        
        </div>
        <div class="modal-footer">       
            <button type="button" class="orange-btn">Save changes</button>
        </div>
        </div>
    </div>
    
    </div>
    <!--/Add New Address -->


    </body>
</html>