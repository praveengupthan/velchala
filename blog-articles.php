<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
    <?php include 'includes/arrayObjects.php'?>
</head>
<body class="animsition">

    <?php include 'includes/header.php' ?>
    <!--main-->   
    <main  class="subpage-main">

    <!-- header sub page -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <h1>Articles</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>   
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Blog</a></li>                   
                    <li class="breadcrumb-item active" aria-current="page"><span>Articles</span></li>
                </ol>
            </nav>
        </div>
        <!--/ container -->
    </div>
    <!--/ hedaer sub page -->

    <!-- container -->
    <div class="container">
       <!-- row -->
       <div class="row justify-content-center">
           <!-- card -->
           <?php 
           for($i=0;$i<count($blogArticles);$i++) {?>
            <div class="col-lg-4">
                <div class="card blogcard articlecard">
                    <div class="card-header">
                        <?php echo $blogArticles[$i][0] ?>
                        <span class="badge badge-success">New</span>
                    </div>
                    <div class="card-body">
                    <p>  <?php echo $blogArticles[$i][1] ?></p>
                    <a href="blog-article-detail.php" class="btn orange-btn">Read More</a>
                    </div>
                </div>              
            </div>
           <?php } ?>
            <!--/ card -->           

       </div>
       <!--/ row -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?> 
   
    </body>
</html>