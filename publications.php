<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php
        include 'includes/arrayObjects.php';
    ?>
</head>
<body class="animsition">

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container wow animate__animated animate__fadeInDown">
                <h1>Publications</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Publications</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
            <!-- publications -->
            <div class="publications-list">
                <!-- sort -->
                <div class="sort">
                   <!-- continainer-->
                   <div class="container">
                       <!-- row -->
                       <div class="row justify-content-between d-none">
                            <!-- col -->
                            <div class="col-md-4 align-self-center">
                                <p class="pb-0">1 – 40 of 75 results</p>
                            </div>
                            <!--/col -->
                             <!-- col -->
                             <div class="col-md-8 publications-filters">
                                <div class="form-group">
                                     <select class="form-control">  
                                        <option>Language</option>                                      
                                         <option>Telugu </option>
                                         <option>English </option>                                                                     
                                     </select>
                                 </div>
                                <div class="form-group">
                                     <select class="form-control">  
                                        <option>Publications</option>                                      
                                         <option>Jayanthi </option>
                                         <option>Sister Niveditha </option>                                                                     
                                     </select>
                                 </div>
                                 <div class="form-group">
                                     <select class="form-control">
                                         <option>Sort by</option>
                                         <option>Popular Books</option>                                       
                                         <option>Newest First</option>   
                                         <option>Alphabet A-Z</option>                                     
                                     </select>
                                 </div>
                             </div>
                            <!--/col -->
                       </div>
                       <!--/row -->
                   </div>
                   <!--/ container --> 
                </div>
                <!--/ sort -->

                <!-- publications list items -->
                <div class="publications-items">
                    <!-- container -->
                    <div class="container">                   
                    <!-- alert wishlist-->
                    <div id="alertAddWishlist" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
                        <strong><span class="icon-check-circle"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your Wishlist.  <a href="javascript:void(0)" class="d-block"><strong>View Now</strong></a>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!--/ alert wishlist -->
                        <!-- row -->
                        <div class="row py-3">
                            <!-- col -->
                            <?php 
                                for($i=0;$i<count($publications);$i++){ ?>
                            <div class="col-6 col-sm-4 col-md-3 wow animate__animated animate__fadeInDown">
                                <div class="book-item">
                                    <figure class="bookcover">
                                        <a target="_blank" href="publication-detail.php">
                                            <img src="img/coverpages/<?php echo $publications[$i][0]?>" alt="" class="img-fluid">
                                        </a>
                                        <!-- <div class="wishlist-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                            <span class="icon-heart-o icomoon"></span>
                                        </div> -->
                                        <!-- <span class="badge badge-pill badge-success">Top Seller</span> -->
                                    </figure>
                                    <article class="text-center">
                                        <a href="publication-detail.php"><?php echo $publications[$i][1]?></a>
                                        <!-- <p class="price text-center">
                                            <span class="offer"><?php echo $publications[$i][2]?></span>
                                            <span class="oldprice"><?php echo $publications[$i][3]?></span>
                                        </p> -->
                                    </article>
                                </div>
                            </div>
                            <?php } ?>
                            <!--/ col -->                           
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row justify-content-center pb-4">
                            <div class="col-lg-4 text-center">
                                <a class="orange-btn" href="javascript:void(0)">Load More</a>
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ container -->
                </div>
                <!--/ publications list items -->
            </div>
            <!--/ publications -->

       </div>
       <!--/ sub page body -->



    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?> 

   <script>
    $(document).ready(function(){        
        $('.wishlist-icon').click(function(){
            $('#alertAddWishlist').show();
        }) 
    });
   </script>
    </body>
</html>