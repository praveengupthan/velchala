<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
    <?php include 'includes/arrayObjects.php'?>
</head>
<body class="animsition">

  <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
    
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Video Gallery</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>   
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Gallery</a></li>                    
                        <li class="breadcrumb-item active" aria-current="page"><span>Videos</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-5">

                 <!-- col -->
                 <?php 
                for($i=0;$i<count($videoAlbums);$i++){ ?>
                 <div class="col-sm-6 col-md-4 text-center video-col wow animate__animated animate__fadeInDown">
                    <iframe width="100%"  src="<?php echo $videoAlbums[$i][0] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <p class="text-left"><?php echo $videoAlbums[$i][1] ?> </p>
                </div>
                <?php } ?>
                <!--/ col -->

                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
       </div>
       <!--/ sub page body -->
       
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>    
    </body>
</html>