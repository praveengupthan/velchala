<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="velchala-page animsition"> 

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">

        <!-- velchala banner -->
        <section class="velchalaBanner">
        <div class="swiper-container velchalaBan">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="img/velBan01.jpg" alt="" class="img-fluid">
                </div>
                <div class="swiper-slide">
                    <img src="img/velBan02.jpg" alt="" class="img-fluid">
                </div>
                <div class="swiper-slide">
                    <img src="img/velBan03.jpg" alt="" class="img-fluid">
                </div>
                <div class="swiper-slide">
                    <img src="img/velBan04.jpg" alt="" class="img-fluid">
                </div>
                <div class="swiper-slide">
                    <img src="img/velBan05.jpg" alt="" class="img-fluid">
                </div>
                <div class="swiper-slide">
                    <img src="img/velBan06.jpg" alt="" class="img-fluid">
                </div>
            </div>  

             <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>  
        </div>

        </section>
        <!--/ velchala banner -->

        <!-- sub page body -->
        <section class="subpage-body velchalaspage">
            <!-- container -->
            <div class="container">
                <!-- single page -->
                <div class="singleScroll">
                <nav>
                    <ul>
                        <li><a href="#section-1">Velchala</a></li>
                        <li><a href="#section-2">Awards</a></li>
                        <li><a href="#section-3">Positions</a></li>
                        <li><a href="#section-4">Author &amp; Translator</a></li>
                        <li><a href="#section-5">Editor</a></li>
                        <li><a href="#section-6">External Visits</a></li>
                        <li><a href="#section-7">Freedom Fighter</a></li>                       
                    </ul>
                </nav>
                <!-- single page body -->
                <div class="singleBody">
                    <!-- section 1-->
                    <section id="section-1" class="wow animate__animated animate__fadeInUp">
                        <h2>Velchala Kondal Rao  </h2>                        
                        <ul class="list-items">
                            <li>A Post Graduate (M.Com.) in Commerce.</li>
                            <li>Was the Principal of three degree colleges and  Director of a Postgraduate college in A.P.</li>
                            <li>Was the Founder Chairman of the Sister Nivedita College of Professional Studies Hyderabad.</li>
                            <li>Was the elected member of the Syndicate of the Osmania University.</li>
                            <li>Was the Joint Director of Higher Education, Govt. of Andhra Pradesh.</li>
                            <li>Was promoted as Director of Higher Education and deputed to Telugu Academy  as Director. </li>
                        </ul>                       
                    </section>
                    <!--/ section 1-->
                    <!-- section 2-->
                    <section id="section-2" class="wow animate__animated animate__fadeInUp">
                        <h2>Awards</h2>
                       <ul class="list-items">
                         <li>Michael Madhusudhan Dutt D.Litt. Award, Calcutta.</li>
                         <li>D.Litt. by World Academy of Arts, California</li>
                         <li>D.Litt. from Potti Sriramulu  Telugu University, Hyderabad.</li>
                         <li>First prize from Telugu University for his translation of Balagangadhara Tilak' s  "Amrutham Kurisina Rathri" from Telugu into English.</li>
                         <li>Pratibha Puraskaram by Sanatana Dharma Charitable Trust of Sadguru Sivananda Murthy on 13th April 2019</li>
                       </ul>
                    </section>
                    <!--/ section 2-->

                    <!-- section 3-->
                    <section id="section-3" class="wow animate__animated animate__fadeInUp">
                        <h2>HON. POSITIONS HELD:</h2>
                        <h4 class="forange">Experience as an Educator</h4>
                        <ul class="list-items">
                         <li>Former President of the Poetry Society of Hyderabad .</li>
                         <li>Hon. Chairman  of the Vishwanatha Saahithya Peetam.</li>
                         <li>Founder President of the Inter-lingual Poetry Society of Hyderabad.</li>
                         <li>Founder Convener of Telangana Educational and Cultural Forum, Hyderabad.</li>
                         <li> Permnent Member of the Poetry Society of India, Delhi.</li>
                         <li>Permanent Member of the Indian Institute of Public Administration, Delhi.</li>
                         <li>Former Vice Chairman of the Forum for Higher Education, Hyderabad.</li>
                       </ul>
                    </section>
                    <!-- /section 3-->

                    <!-- section 4-->
                    <section id="section-4" class="wow animate__animated animate__fadeInUp">
                        <h2>Author &amp; Translator</h2>                        
                        <ul class="list-items">
                            <li>Authored 40 books in Telugu and English and one book in Urdu.</li>
                            <li>Translated 10 books from Telugu, English and Urdu. </li>
                            <li>Edited 10 books in English and Telugu including a book on "Telangana Struggle for Identity"</li>
                            <li>Published one coffee table book by name "Telangana - A Classic Cultural Khazana"</li>
                            <li>Knows Telugu, English, Urdu and Hindi languages and writes and translates in English, Telugu and Urdu.</li>
                            <li>Has written more than 60 articles in different journals and dailies in Telugu and English.</li>
                        </ul>
                    </section>
                    <!-- /section 4-->

                    <!-- section 5-->
                    <section id="section-5" class="wow animate__animated animate__fadeInUp">
                        <h2>Editor</h2>
                        <ul class="list-items">
                            <li>Former editor of 'Shatavahana' of Shatavahana  PG Centre, Karimnagar</li>
                            <li>Former Editor of 'Telugu' Journal of Telugu Akademi, Hyderabad.</li>
                            <li>Presently Chief Editor of the multi-lingual quarterly journal "Viswanadha Jayanthi" and  quarterly management journal "Business Vision", Hyderabad.</li>                            
                        </ul>
                        
                    </section>
                    <!-- /section 5-->

                    <!-- section 6-->
                    <section id="section-6" class="wow animate__animated animate__fadeInUp">
                        <h2>External Visits</h2>
                        <ul class="list-items">
                            <li>Visited England, U.S.A., Japan, Taiwan,   Turkey and also participated in International Poets Meets held at Bangkok and Istanbul.</li>
                        </ul>
                    </section>
                    <!-- /section 6-->

                    <!-- section 7-->
                    <section id="section-7" class="wow animate__animated animate__fadeInUp">
                        <h2>Freedom Fighter</h2>
                        <ul class="list-items">
                            <li>Participated in Indian Independence Struggle, Hyderabad Liberation Struggle and Telangana Identity Struggle.</li>
                            <li>Is the recipient of TAMARAPATRA from Govt. of India for participation in Independent struggle.</li>
                        </ul>

                        <ul class="list-items">
                            <li>Was born on twenty first of July nineteen thirty two (21st July 1932) at Karimnagar of Telangana state.</li>
                            <li>Is married to V.Nirmala and blessed with daughters P.Rama, R.Uma and  son Agam.</li>
                            <li>Residing at 202, My Home Madhuban Apts., Srinagar Colony, Hyderabad, Telangana, India</li>
                            <li>Office : Viswanatha Sahitya Peetam, 11-4-654/2, Near Nelofur Hospital, Red Hills, Lakdikapul, Hyderabad. Ph. : 23395358</li>
                            <li>Cell number : 98481 95959, 9100443876,  Email: krvelchala2012@gmail.com</li>
                        </ul>
                    </section>
                    <!-- /section 7-->
                   
                </div>
                <!--/ single page body -->
                </div>
                <!--/ single scroll ends -->
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    

   <?php include 'includes/scripts.php' ?>  

</body>
</html>