<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Contact</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Contact</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
          <!-- container -->
          <div class="container">
                <!-- row -->
                <div class="row py-5">
                    <!-- col -->
                    <div class="col-lg-3 col-sm-6 col-md-6 contact-col text-center wow animate__animated animate__fadeInDown" >
                        <div class="icon-col">
                            <a href="javascript:void(0)" target="_blank"><span class="icon-pin icomoon"></span></a>
                        </div>
                        <p class="text-center">Namrata Estates, Countryoven Lane, Greenlands, Ameerpet road, Hyderabad</p>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-6 col-md-6 contact-col text-center wow animate__animated animate__fadeInUp" >
                        <div class="icon-col">
                        <a href="tel:+918431414707"> <span class="icon-phone icomoon"></span></a>
                        </div>
                        <p class="text-center mb-0 pb-0">+918026589777</p>
                        <p class="text-center">+918431414707</p>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-6 col-md-6 contact-col text-center wow animate__animated animate__fadeInDown">
                        <div class="icon-col">
                            <span class="icon-globe icomoon"></span>
                        </div>
                        <p class="text-center mb-0 pb-0">
                            <a href="mailto:support@velchala.com">support@velchala.com</a>
                        </p>
                        <p class="text-center">www.velchala.com</p>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-6 col-md-6 contact-col text-center wow animate__animated animate__fadeInUp">
                        <div class="icon-col">
                            <span class="icon-clock icomoon"></span>
                        </div>
                        <p class="text-center mb-0 pb-0"> Monday - Friday 9 AM - 7PM </p>
                        <p class="text-center">Saturday  9 AM - 5PM</p>
                    </div>
                    <!--/ col -->  
                </div>
                <!--/ row -->

                <form>
                    <!-- row -->
                    <div class="row whitebox py-4 contact-form wow animate__animated animate__fadeInDown">
                        <!-- col -->
                        <div class="col-lg-12">
                            <h3 class="h4 fsbold">Reach us</h3>
                            <p class="pb-2">Send the following details, our Team will contact you shortly</p>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class="bmd-label-floating">Name</label>
                                <div class="input-group">
                                <input type="text" class="form-control" id="name" placeholder="Enter your name">       
                                </div>                        
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="email" class="bmd-label-floating">Email</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="email" placeholder="Enter your Valied Email">          
                                </div>                     
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="contactNumber" class="bmd-label-floating">Contact Number</label>
                                <div class="input-group">
                                <input type="text" class="form-control" id="contactNumber" placeholder="Contact Number">     
                                </div>                          
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="subject" class="bmd-label-floating">Subject</label>
                                <div class="input-group">
                                <input type="text" class="form-control" id="subject" placeholder="Subject (Optional)">       
                                </div>                        
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="cityState" class="bmd-label-floating">City / State</label>
                                <div class="input-group">
                                <input type="text" class="form-control" id="cityState" placeholder="Ex: Hyderabad, Telangana">     
                                </div>                          
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-md-12">
                            <div class="form-group">
                                <label for="message" class="bmd-label-floating">Message</label>
                                <div class="input-group">
                                    <textarea class="form-control" id="message" rows="3" placeholder="Enter Message"></textarea>
                                </div>                         
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-md-12">
                            <div class="form-check form-group">
                                    <input style="margin-top:8px" type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label  class="form-check-label" for="exampleCheck1">I want to be Stockist / Representative to your Publications </label>
                            </div>
                        </div>
                        <!--/ col -->

                        <div class="col-md-12 pt-2">
                            <input type="submit" class="orange-btn" value="Submit Request">
                        </div>
                        
                    </div>
                    <!--/ row -->
                </form>
          </div>
          <!--/ container -->
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includesfooter.php' ?>
    <?php include 'includes/scripts.php' ?> 
    </body>
</html>