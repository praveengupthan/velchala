<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
    <?php include 'includes/arrayObjects.php'?>
</head>
<body class="animsition">

    <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
     <!-- header sub page -->
     <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Events</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>   
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Blog</a></li>                   
                        <li class="breadcrumb-item active" aria-current="page"><span>Events</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row py-3">
            <!-- col -->
            <?php
                for($i=0;$i<count($blogEventItem);$i++) {?>
                <div class="col-md-6 col-lg-4 wow animate__animated animate__fadeInDown">
                    <div class="card blogcard">
                        <a href="blog-events-detail.php">
                            <img class="card-img-top img-fluid" src="img/albums/<?php echo $blogEventItem[$i][0]?>.jpg">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title ptregular"><?php echo $blogEventItem[$i][1]?></h5>
                            <p class="card-text pb-3"><?php echo $blogEventItem[$i][2]?></p>
                            <p><?php echo $blogEventItem[$i][3]?> <span class="d-inline-block px-3 small pb-3">|</span><?php echo $blogEventItem[$i][4]?></p>
                            <a href="blog-events-detail.php" class="btn orange-btn">Read More</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!--/ col -->                  
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

    <?php include 'includes/footer.php'?>
   <?php include 'includes/scripts.php' ?> 
   
    </body>
</html>