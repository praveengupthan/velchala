<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

   <?php include 'includes/headerTelugu.php' ?>
    <!--main-->   
    <main class="subpage-main">
        <!-- figure -->
        <img src="img/video-bg.jpg" class="img-fluid vspimg" alt="">
        <!--/ figure -->

        <!-- top vsp description -->
        <div class="top-vsp">
            <!-- container -->
            <div class="container whitebox">
                <!-- row -->
                <div class="row py-5 wow animate__animated animate__fadeInDown">                  
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <article class="pl-md-5">
                            <p class="fblue"><i>"Art is to Discover a Flash Extraordinary  in an Object Ordinary"</i></p>
                        </article>
                    </div>
                    <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 border-left">
                            <h1 class="ptregular pl-3">విశ్వనాథ సాహిత్య పీఠం</h1>
                        </div>
                    <!--/ col -->   
                    <!-- col -->
                    <div class="col-lg-12 py-4">
                        <h5 class="ptregular h5 fblue text-center">
                        <i> సిస్టర్ నివేదిత ఫౌండేషన్ పక్షాన స్థాపించబడిన ఈ పీఠం దిగువ ఉదహరించిన లక్ష్యాలతో సాహిత్య, సంస్కృతి  పరమైన కార్యకలాపాలు జులై 2003 నుండి నిర్వహించడం జరుగుతున్నది.</i>
                        </h5>
                    </div>
                    <!--/ col -->                
                </div>
                <!--/ row -->
                
            </div>
            <!--/ container -->
        </div>
        <!--/ top vsp description -->

        <!-- orange section -->
        <div class="orangebg py-3 my-3">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6 text-center vspban wow animate__animated animate__fadeInDown">
                        <img src="img/kavi_samraat.jpg" alt="">
                        <h2 class="h1 ptregular fwhite">ఎ విజన్ అండ్ ప్రొజెక్షన్</h2>
                        <p class="fwhite">
                            <i>I am convinced also of this, the heart that must rule humanity is replaced by the mind. It is a bane""</i>
                        </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ orange section -->

        <div class="vspmaster">
            <!-- container -->
            <div class="container">
                 <!-- row -->
                 <div class="row py-4">
                    <!-- col -->
                    <div class="col-lg-6">
                    <h3 class="h3 pb-3">లక్ష్యాలు</h3>
                        
                        <ul class="list-items">
                            <li>విశ్వనాథ  రచనలను  సమాజంలో అందరికి అందుబాటులోకి తేవటం, దీనికనుగుణంగా సాహిత్య, సాంస్కృతిక కార్యక్రమాలు చేపట్టడం.</li>
                            <li>విశ్వనాథ గ్రంధాలయ స్థాపన: ఇందులో విశ్వనాథ  రచనలు, లేఖలు, ప్రసంగాలు, ఇంటర్వ్యూలతో పాటు వారి రచనలపై వఛ్చిన పరిశోధనలు, విమర్శలు, కావ్యాలు, ప్రసంశలు, పరిశోధకులకు సహాయ ప్రొత్సాహాలందిచ్ఛే ప్రధానాంశాలతో నిక్షేపించడం.</li>
                            <li>విశ్వనాథ రచనలు ఇతర భాషల్లోకి అనువదింపచేయడం.</li>
                            <li> ప్రతి సంవత్సరం తెలుగులో ఒక రచయిత/రచయిత్రిని విశ్వనాథ సాహిత్య పురస్కారంతో సన్మానించడం. దీనికోసం  ప్రతిభా నిర్ణాయక సంఘాన్ని స్థాపించి ఎంపిక చేయడం.</li>
                            <li> 'జయంతి' అన్న పేరున ఒక త్రైమాసిక సాహిత్య పత్రిక ప్రచురించటం, రచనల కొరకు సాహిత్యంలో పోటీలు, గోష్టులు, సమీక్షలు, సమావేశాలు నిర్వహించి వ్యాసాలు  వగైరా ఎంపిక చేయడం.</li>
                            <li>విశ్వనాథ రచనల పఠనా  అవగాహన కొరకు ఏడాదికొకసారి రాష్ట్రంలోని వివిధ ప్రాంతాల్లో, సాహిత్య సాంస్క్రితిక  సమావేశాలు, సభలు నిర్వహించడం.</li>
                        </ul>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-6">                       
                        <ul class="list-items">
                            <li>ఏడాదికొకసారి విశ్వనాథ సాహిత్య సృష్టిలో ఏదో ఒక ప్రక్రియపై సాహిత్య గోష్టి  ఏర్పాటు ఏర్పాటు చేయటం.</li>
                            <li>విశ్వనాథ సాహితీ ప్రక్రియల్లో ఏదో ఒకదానిపై ఏడాదికొకసారి వ్యాస రచన పోటీలు నిర్వహించి వారి జన్మదినోత్సవం నాడు ఉత్తమ వ్యాస రచయిత/రచయిత్రిని సన్మానించడం.</li>
                            <li>విశ్వనాథ రచనల్లో ఏదో ఒక రచనపై నెలకొకసారి ఉపన్యాస కార్యక్రమం హైద్రాబాద్లో కానీ, వేరేచోట కానీ నిర్వహించడం.</li>
                            <li>"కవిసామ్రాట్ విశ్వనాథ సత్యనారాయణ డీమ్డ్ యూనివర్సిటీ ఫర్ కల్చరల్ స్టడీస్" అన్నదానిని హైద్రాబాద్ లో స్థాపించడం.</li>
                            <li>విశ్వనాథ  విగ్రహాన్నితదితర మహనీయుల ప్రక్కన హైద్రాబాద్లోని ట్యాంకుబండు వద్ద నెలకొల్పుటకు "రెప్రెసెంత్" చేయడం.</li>   
                            <li>ప్రతియేట ప్రసిద్ధుడైన ఒక పండితుణ్ణి ఆహ్వానించి  విశ్వనాథ  వారి జన్మదినం రోజున విశ్వనాథ  స్మారక ఉపన్యాసం ఇప్పించడం.</li>                         
                        </ul>
                        <p>ఈ లక్ష్యాల సాధన కొరకు విశ్వనాథ సాహిత్య పీఠాన్ని స్థాపించడం జరిగింది.</p>
                       
                    </div>
                    <!--/ col -->
                    
                </div>
                <!--/ row -->
            </div>
            <!--/container -->
        </div>
    </main> 
    <!--/ main-->
    <?php include 'includes/footerTelugu.php' ?>
   <?php include 'includes/scripts.php' ?> 
    </body>
</html>