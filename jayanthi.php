<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
    <?php
        include 'includes/arrayObjects.php';
    ?>
</head>
<body class="animsition">

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Viswanadha Jayanti Quarterly Magazine </h1>              
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Magazines</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
            <!-- publications -->
            <div class="publications-list">
                <!-- sort -->
                <div class="sort">
                   <!-- continainer-->
                   <div class="container">
                       <!-- row -->
                       <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-sm-6 align-self-center">
                                <p class="pb-0">1 – 12 of 25 results</p>
                            </div>
                            <!--/col -->
                             <!-- col -->
                             <div class="col-sm-6 justify-content-end">
                                 <div class="form-group mb-0 float-right">
                                     <select class="form-control">
                                         <option>Choose Year</option>
                                         <option>2015</option>
                                         <option>2016</option>
                                         <option>2017</option>
                                         <option>2018</option>   
                                         <option>2019</option>                                     
                                     </select>
                                 </div>
                             </div>
                            <!--/col -->
                       </div>
                       <!--/row -->
                   </div>
                   <!--/ container --> 
                </div>
                <!--/ sort -->

                <!-- publications list items -->
                <div class="publications-items">
                    <!-- container -->
                    <div class="container">
                        <!-- row -->
                        <div class="row py-3">
                            <!-- col -->
                            <?php 
                            for($i=0;$i<count($jayanthiCover);$i++) { ?>
                            <div class="col-6 col-sm-4 col-md-3 wow animate__animated animate__fadeInUp">
                                <div class="book-item">
                                    <figure class="bookcover">
                                        <a href="jayanthi-detail.php" target="_blank">
                                            <img src="img/coverjayanthi/<?php echo $jayanthiCover[$i][0]?>.jpg" alt="" class="img-fluid">
                                        </a>  
                                    </figure>
                                    <article class="text-center">
                                        <a href="jayanthi-detail.php" target="_blank"><?php echo $jayanthiCover[$i][1]?></a>
                                    </article>
                                </div>
                            </div>
                            <?php } ?>
                            <!--/ col -->                                                   
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ container -->
                </div>
                <!--/ publications list items -->
            </div>
            <!--/ publications -->
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?> 
    </body>
</html>