<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
    <?php include 'includes/arrayObjects.php'?>
</head>
<body class="animsition">

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- <h1>Publications</h1> -->
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="publications.php">Publications</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Book Name will be here</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

            <!-- top publication detail -->
            <div class="book-top-detail">
                <!-- container -->
                <div class="container">
                   
                    <!-- card-->
                    <div class="card p-2 p-sm-5">
                        <!-- alert wishlist-->
                        <div id="alertAddWishlist-Detail" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
                            <strong><span class="icon-check"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your Wishlist.  <a class="d-block" href="user-wishlist.php"><strong>View Now</strong></a>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!--/ alert wishlist -->

                         <!-- alert add to cart-->
                         <div id="alertAddtocart" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
                            <strong><span class="icon-check"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your cartlist.  <a class="d-block" href="cart.php"><strong>View Now</strong></a>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!--/ alert add to cart -->


                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 wow animate__animated animate__fadeInDown">
                                <figure class="figure-detail">
                                    <img src="img/coverpages/cover01.jpg" alt="" class="img-fluid">
                                </figure>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-6 book-detail-rt align-self-center">
                                 <!-- border -->
                                <div class="px-4 py-3 border wow animate__animated animate__fadeInUp">
                                    <!-- row -->
                                    <div class="row position-relative">
                                        <div class="col-md-9 col-12">
                                            <h1 class="h2 ptregular">Viswanatha Jayanthi</h1>
                                            <p class="pb-1">
                                                <span class="badge badge-success p-1">4.4 <span class="icon-star icomoon"></span> </span>
                                                <span class="fgray">(25 Reviews)</span>
                                            </p>
                                            <p class="price pl-1">
                                                <span class="offer">350</span>
                                                <span class="oldprice">400</span>
                                            </p>
                                        </div>
                                        <div class="col-md-3 col-12 text-right ">
                                            <div class="wishlist-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                                <span class="icon-heart-o icomoon"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ row -->
                                    <p class="basic-description">Pellentesque dolor augue, euismod vel orci in, congue sodales nunc. Duis sed auctorol or, ac facilisis mi. Duis malesuada, arcu ut ultricies. </p>
                                </div>
                                <!-- border/-->

                                <!-- border -->
                                <div class="px-4 border mt-2 wow animate__animated animate__fadeInDown">
                                    <!-- specifications -->
                                    <div class="book-specs">
                                     <!-- row -->
                                     <div class="row">
                                         <!--col-->
                                         <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Availability</dt>
                                                    <dd class="fgreen fsbold">In Stock</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 brd-rt brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>SKU Code</dt>
                                                    <dd>CA78963</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Pages</dt>
                                                    <dd>215</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Author</dt>
                                                    <dd>Dr. Velchala</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 brd-rt brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Language</dt>
                                                    <dd>Telugu</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Publisher</dt>
                                                    <dd>Sister Niveditha</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Published Date</dt>
                                                    <dd>11-05-2012</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>No. of Readers</dt>
                                                    <dd>25</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                     </div>
                                     <!--/ row -->
                                 </div>
                                 <!--/ specifications -->
                                </div>
                                <!--/ border -->

                                <p class="py-3">
                                    <span>QTY</span>
                                    <span>
                                        <input class="text-center qty" type="number" placeholder="1">
                                    </span>
                                </p>

                                <a href="img/readbook.pdf" class="orange-btn text-uppercase" target="_blank">Read Online</a>
                                <a id="addtokcart-icon" href="javascript:void(0)" class="orange-btn-border text-uppercase">Add to Cart</a>

                                <!-- social share -->
                                <div class="social-share pt-5">
                                    <span class="pt-1 span-share">Share this product</span>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-linkedin-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-instagram icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-pinterest icomoon"></span></a>
                                </div>
                                <!--/ social share -->
                                
                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/card ends -->

                    <!-- responsive tab -->
                    <div class="custom-tab">
                        <!-- tab -->
                        <div class="parentHorizontalTab">
                            <ul class="resp-tabs-list hor_1 wow animate__animated animate__fadeInUp">
                                <li>Description</li>
                                <li>Reviews</li>
                                <li>Videos</li>
                            </ul>
                            <div class="resp-tabs-container hor_1 wow animate__animated animate__fadeInUp">
                                <!-- description -->
                                <div>
                                    <p>Lorem ipsum dolor sit amet, qui sumo copiosae ea, vim dolorum volumus ei. Mei ei dolor tamquam deleniti. Habemus copiosae his cu, quo ne reque augue. Nonumy contentiones his eu, vel falli copiosae inimicus te. Liber commune et vix. Putant quaeque inimicus cu usu, eum civibus delicatissimi an. Tota suavitate mnesarchum ad nec, at vim propriae voluptua. In vim laudem cetero interpretaris. Civibus vivendum conclusionemque mel eu. Homero dolorum phaedrum sed no, eos oratio forensibus te. Mea possim accusamus theophrastus ex, ea dicit patrioque sit. Vix possim volutpat suscipiantur cu, ex nam purto invidunt, ut qui nominati pertinacia.</p>
                                    <p>Lorem ipsum dolor sit amet, qui sumo copiosae ea, vim dolorum volumus ei. Mei ei dolor tamquam deleniti. Habemus copiosae his cu, quo ne reque augue. Nonumy contentiones his eu, vel falli copiosae inimicus te. Liber commune et vix. Putant quaeque inimicus cu usu.</p>
                                </div>
                                <!--/ description -->

                                <!-- reviews -->
                                <div>
                                    <h2 class="h4 ptregular pb-3 border-bottom">Customer Reviews <span>(20)</span></h2>
                                    <!-- user review -->
                                    <div class="user-reviewrow border-bottom">
                                        <figure>
                                            <img class="sm-thumb" src="img/users-thumb/thumb01.jpg" alt="">
                                        </figure>
                                        <div class="user-details-review">
                                            <h4 class="h5 fsbold">SARENA DOE  <span>2 days ago</span></h4>
                                            <ul class="stars-rating d-flex">
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                            </ul>
                                            <p>Nam ut egestas nibh. Phasellus sollicitudin tempus neque quis gravida. Aenean a eros at ex pharetra suscipit. Proin iaculis ipsum ac ullamcorper pretium. Morbi ut leo eu felis commodo porta.</p>
                                        </div>
                                    </div>
                                    <!--/ user review -->

                                    <!-- user review -->
                                    <div class="user-reviewrow border-bottom">
                                        <figure>
                                            <img class="sm-thumb" src="img/users-thumb/thumb02.jpg" alt="">
                                        </figure>
                                        <div class="user-details-review">
                                            <h4 class="h5 fsbold">SARENA DOE  <span>4 days ago</span></h4>
                                            <ul class="stars-rating d-flex">
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                            </ul>
                                            <p>Nam ut egestas nibh. Phasellus sollicitudin tempus neque quis gravida. Aenean a eros at ex pharetra suscipit. Proin iaculis ipsum ac ullamcorper pretium. Morbi ut leo eu felis commodo porta.</p>
                                        </div>
                                    </div>
                                    <!--/ user review -->

                                     <!-- user review -->
                                     <div class="user-reviewrow border-bottom">
                                        <figure>
                                            <img class="sm-thumb" src="img/users-thumb/thumb03.jpg" alt="">
                                        </figure>
                                        <div class="user-details-review">
                                            <h4 class="h5 fsbold">SARENA DOE  <span>4 days ago</span></h4>
                                            <ul class="stars-rating d-flex">
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                            </ul>
                                            <p>Nam ut egestas nibh. Phasellus sollicitudin tempus neque quis gravida. Aenean a eros at ex pharetra suscipit. Proin iaculis ipsum ac ullamcorper pretium. Morbi ut leo eu felis commodo porta.</p>
                                        </div>
                                    </div>
                                    <!--/ user review -->

                                     <!-- user review -->
                                     <div class="user-reviewrow border-bottom">
                                        <figure>
                                            <img class="sm-thumb" src="img/users-thumb/thumb04.jpg" alt="">
                                        </figure>
                                        <div class="user-details-review">
                                            <h4 class="h5 fsbold">SARENA DOE  <span>4 days ago</span></h4>
                                            <ul class="stars-rating d-flex">
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                            </ul>
                                            <p>Nam ut egestas nibh. Phasellus sollicitudin tempus neque quis gravida. Aenean a eros at ex pharetra suscipit. Proin iaculis ipsum ac ullamcorper pretium. Morbi ut leo eu felis commodo porta.</p>
                                        </div>
                                    </div>
                                    <!--/ user review -->

                                     <!-- user review -->
                                     <div class="user-reviewrow border-bottom">
                                        <figure>
                                            <img class="sm-thumb" src="img/users-thumb/default.jpg" alt="">
                                        </figure>
                                        <div class="user-details-review">
                                            <h4 class="h5 fsbold">SARENA DOE  <span>4 days ago</span></h4>
                                            <ul class="stars-rating d-flex">
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                            </ul>
                                            <p>Nam ut egestas nibh. Phasellus sollicitudin tempus neque quis gravida. Aenean a eros at ex pharetra suscipit. Proin iaculis ipsum ac ullamcorper pretium. Morbi ut leo eu felis commodo porta.</p>
                                        </div>
                                    </div>
                                    <!--/ user review -->

                                     <!-- user review -->
                                     <div class="user-reviewrow border-bottom">
                                        <figure>
                                            <img class="sm-thumb" src="img/users-thumb/thumb06.jpg" alt="">
                                        </figure>
                                        <div class="user-details-review">
                                            <h4 class="h5 fsbold">SARENA DOE  <span>4 days ago</span></h4>
                                            <ul class="stars-rating d-flex">
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                                <li><span class="icon-star-o icomoon"></span></li>
                                            </ul>
                                            <p>Nam ut egestas nibh. Phasellus sollicitudin tempus neque quis gravida. Aenean a eros at ex pharetra suscipit. Proin iaculis ipsum ac ullamcorper pretium. Morbi ut leo eu felis commodo porta.</p>
                                        </div>
                                    </div>
                                    <!--/ user review -->
                                     <!-- leave review -->
                                     <div class="leave-review">
                                        <form class="form">                                           
                                            <div class="form-group">                                               
                                                <textarea class="form-control" placeholder="Leave Your Review"></textarea>
                                            </div>
                                            <div class="rating"></div>
                                            <p><small>You have given rating 4/5</small></p>
                                        </form>
                                        <a href="javascript:void(0)" class="orange-btn-border text-uppercase">Submit REview</a>

                                    </div>
                                    <!--/ leave review -->
                                </div>
                                <!--/ reviews -->

                                <!-- related videos -->
                                <div>
                                    <h3 class="h4 ptregular pb-3 border-bottom">Related videos <span>(5)</span></h3>
                                     <!-- row -->
                                     <?php 
                                     if(count($videoAlbums)>0) {?>
                                     <div class="row justify-content-center py-3">
                                        <!-- col -->
                                        <?php
                                        for($i=0;$i<3;$i++) {?>
                                        <div class="col-md-4 text-center video-col">
                                            <iframe width="100%"  src="<?php echo $videoAlbums[$i][0]?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p class="text-left"><?php echo $videoAlbums[$i][1]?></p>
                                        </div>
                                            <?php } ?>
                                        <!--/ col -->                                        
                                    </div>
                                     <!--/row -->
                                        <?php }  else {?>
                                   
                                     <!-- row -->
                                     <div class="row justify-content-center py-3 novideos-row">
                                        <!-- col -->
                                        <div class="col-lg-6 text-center">
                                            <span class="icon-no-video icomoon"></span>
                                            <p class="text-center">No Videos Available in this Publication</p>
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/row -->
                                    <?php } ?>
                                </div>
                                <!--/ related videos -->
                            </div>
                        </div>
                        <!--/ tab -->
                    </div>
                    <!--/ responsive tab -->

                    <!-- you may also interested in -->
                    <div class="similar-books wow animate__animated animate__fadeInUp">
                        <h2 class="h3 ptregular border-bottom py-3">You may also be interested in</h2>
                        <!-- row -->
                        <div class="row pt-3">
                              <!-- col -->
                              
                              <?php 
                              for($i=0;$i<4;$i++) {?>
                               <div class="col-6 col-sm-4 col-md-3 wow animate__animated animate__fadeInDown">
                                <div class="book-item">
                                    <figure class="bookcover">
                                        <a href="publication-detail.php">
                                            <img src="img/coverpages/<?php echo $publications[$i][0]?>" alt="" class="img-fluid">
                                        </a>
                                        <div class="wishlist-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                            <span class="icon-heart-o icomoon"></span>
                                        </div>
                                        <span class="badge badge-pill badge-success">Top Seller</span>
                                    </figure>
                                    <article class="text-center">
                                        <a href="publication-detail.php"><?php echo $publications[$i][1]?></a>
                                        <p class="price text-center">
                                            <span class="offer"><?php echo $publications[$i][2]?></span>
                                            <span class="oldprice"><?php echo $publications[$i][3]?></span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <?php } ?>
                           
                            <!--/ col -->                           
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ you may also interested in -->
                </div>
                <!--/ container-->
            </div>
            <!--/ top publication detail -->

           
           
                

       </div>
       <!--/ sub page body -->



    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
   <?php include 'includes/scripts.php' ?> 

   <script>
    $(document).ready(function(){        
        $('.wishlist-icon').click(function(){
            $('#alertAddWishlist-Detail').show();
        });

        $('#addtokcart-icon').click(function(){
            $('#alertAddtocart').show();
        }) 
    });
   </script>
    </body>
</html>