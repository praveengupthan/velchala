<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

   <?php
        include 'includes/headerTelugu.php';
   ?>
   <?php
        include 'includes/arrayObjects.php';
    ?>

    <!--main-->    
	<!-- Hero section -->
	<section class="home-slider">   
        <div class="ashokaChakra">
            <img src="img/ashokaChakra.svg" alt=>
        </div>  

         <!-- video -->
        <div class="video-section">
            <div class="text-video">
                <h1 class="mask">సుస్వాగతం <br> వెల్చాల.కామ్  </h1>
            </div>           
            <div class="video-background"></div>
          
        </div>
        <!--/ video -->		
	
	</section>
    <!-- Hero section end -->
        
    <!-- great people-->
    <div class="great-people">
        <!-- Swiper -->
       
        <div class="swiper-container greatpeople-in">
            <div class="swiper-wrapper">
                <?php
                    for($i=0;$i<count($greatPeople);$i++){ ?>
                    <div class="swiper-slide"><img src="img/great/<?php echo $greatPeople[$i][0]?>" alt="<?php echo $greatPeople[$i][0]?>"></div>       
                <?php } ?>       
            </div>
        <!-- Add Pagination -->
        <!-- <div class="swiper-pagination"></div> -->
        </div>
    </div>
    <!--/ great people-->

    <!-- vsp and velchala -->
    <div class="vspsection">
        <!-- container fluid -->
        <div class="container-fluid px-0">
            <!-- row -->
            <div class="row no-gutters">
                <!-- col -->
                <div class="col-12 col-sm-12 col-md-6 vspcol vspleft wow animate__animated animate__fadeInUp">
                <div class="img-div align-self-center">
                        <img src="img/velchalapng.png" alt="" class="img-fluid">
                    </div>
                    <div class="article-div align-self-center">
                        <h6 class="h6">డా. వెల్చాల కొండల రావు </h6>
                        <h3 class="h5 py-3">వ్యక్తిగత వివరాలు</h3>
                        <p class="pb-3">తెలుగు అకాడెమీ మాజీ సంచాలకులు,'జయంతి' సాహిత్య సాంస్కృతిక త్రైమాసిక పత్రిక సంపాదకులు, పోయెట్రీ సొసైటీ అఫ్ ఇండియా జీవిత సభ్యులు, పోయెట్రీ సొసైటీ అఫ్ హైదరాబాద్ సభ్యులు, ఇండియన్ ఇన్స్టిట్యూట్ అఫ్ పబ్లిక్ అడ్మినిస్ట్రేషన్  పర్మనెంట్ సభ్యులు, ఫోరమ్ ఫర్ హయ్యర్ ఎడ్యుకేషన్, హైదరాబాద్ ఉపాధ్యక్షులు,  తెలుగు,ఇంగ్లీష్, ఉర్దూ భాషల్లో రచయిత, అనువాదకుడు, సంకలనకర్త.</p>
                        <a href="velchalaT.php" class="orange-btn">ఇంకా చదవండి</a>
                    </div>
                   
                </div>
                <!--/ col -->
               <!-- col -->
               <div class="col-12 col-sm-12 col-md-6 vspcol vsprt wow animate__animated animate__fadeInDown">
               <div class="img-div">
                    <img src="img/vsp-png.png" alt="" class="img-fluid">
                </div>
                <div class="article-div align-self-center">
                    <h6 class="h6"> స్థాపించిన వారు డా. వెల్చాల కొండల రావు   </h6>
                    <h3 class="h5 py-3">విశ్వనాథ సాహిత్య పీఠం</h3>
                    <p class="pb-3">విశ్వనాథ సత్యనారాయణ పేరుతో "విశ్వనాథ సాహిత్య పీఠం" జులై 2003లో ప్రారంభమైంది. కవులు, రచయితల పేరు మీద సాహితీ సంస్థలు, పీఠాలు  నెలకొల్పడం సహజమే. అయితే విశ్వనాథ మరణం తరువాత మూడు  దశాబ్దాలకు ఈ పీఠం ఏర్పడడం వెనుక దాదాపు మూడు దశాబ్దాల పాటు సాగిన సుదీర్ఘ అంతర్మథన, ఆలోచనల, సంప్రదింపుల సమాహారం ఉంది. </p>
                    <a href="vspT.php" class="orange-btn">ఇంకా చదవండి</a>
                </div>
                
            </div>
            <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->
    </div>
    <!-- /vsp and velchala -->

    <!-- publications-->
    <div class="publications">
        <!-- custom container -->
        <div class="cust-container">
             <!-- title -->
             <div class="title-section wow animate__animated animate__fadeIn">
                <h4 class="h4">ప్రచురణలు</h4>
                <p>డా. వెల్చాల కొండల రావు రచన</p>               
            </div>
            <!--/ title -->
           
            <!-- books publications -->           
            <div class="swiper-container home-publications">
                <div class="swiper-wrapper ">
                    <!-- slide -->
                    <?php 
                    for($i=0;$i<count($homeBooks);$i++) {?>
                    <div class="swiper-slide">
                        <div class="img-box">
                            <img src="img/coverpages/<?php echo $homeBooks[$i][0]?>" alt="" class="img-fluid">
                            <!--hover -->
                            <div class="hover-section">                               
                                <a href="publication-detail.php"><span class="icon-search icomoon"></span></a>
                            </div>
                            <!--/ hover-->
                        </div>                        
                    </div>
                    <?php } ?>
                    <!--/ slide -->  
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
            <!-- /books publications -->
        </div>
        <!--/ custom container -->
        <p class="text-center pt-5">
            <a href="publications.php" class="orange-btn mx-auto wow animate__animated animate__slideInUp">మరిన్ని చూడండి</a>
       </p>
    </div>
    <!--/ publications -->

    <!-- gallery -->
    <div class="home-gallery">
        <!-- custom container -->
        <div class="cust-container gallery-block grid-gallery">
            <!-- title -->
            <div class="title-section  wow animate__animated animate__slideInUp">
                <h4 class="h4">గ్యాలరీ</h4>
                <p>చిత్రాల గ్యాలరీ డా. వెల్చాల కొండల రావు</p>               
            </div>
            <!--/ title -->

            <!-- gallery images -->
           <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- item -->
                    <?php 
                        for($i=0;$i<count($homeGallery);$i++){ ?>
                    <div class="col-6 col-sm-6 col-md-3 item  wow animate__animated animate__fadeInUp">
                        <a class="lightbox" href="img/gallery/<?php echo $homeGallery[$i][0]?>">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/<?php echo $homeGallery[$i][0]?>">
                        </a>
                    </div>
                        <?php } ?>
                    <!-- item -->                    
                </div>
                <!--/row -->               
           </div>
            <!--/ gallery images -->
           <p class="text-center pt-3">
                <a href="photo-albums.php" class="orange-btn mx-auto  wow animate__animated animate__fadeIn">అన్ని ఫోటో గ్యాలరీని చూడండి</a>
           </p>
        </div>
        <!--/ customcontainer -->        
    </div>
    <!--/ gallery -->

    <!-- poems -->
    <div class="home-poems">
        <!-- custome container -->
        <div class="cust-container">
            <h5 class="h5 wow animate__animated animate__slideInDown">వెల్చల రాసిన కవితలు</h5>

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center wow animate__animated animate__slideInUp">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <!-- swiper -->
                        <!-- Swiper -->
                            <div class="swiper-container poems-container">
                                <div class="swiper-wrapper">
                                    <?php 
                                        for($i=0;$i<count($homePoemsTelugu);$i++) { ?>
                                    <div class="swiper-slide">
                                        <article>
                                            <p>" <?php echo $homePoemsTelugu[$i][0]?>"</p>
                                        </article>
                                    </div>   
                                        <?php } ?>                                                    
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                        <!--/ swiper -->
                    </div>
                    <!-- col -->
                </div>
                <!--/row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ custom container -->
    </div>
    <!--/ poems -->
    
    <!--/ main-->
    <?php include 'includes/footerTelugu.php' ?>   

   <?php include 'includes/scripts.php' ?>
    <!-- hero slider -->
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/owl.carousel.min.js"></script> 
    <script src="js/jquery.nicescroll.min.js"></script> 
    <script src="js/main.js" ></script>  
</body>
</html>