<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

   <?php
        include 'includes/header.php';
   ?>
   <?php
        include 'includes/arrayObjects.php';
    ?>

    <!--main-->    
	<!-- Hero section -->
	<section class="home-slider">   
         <!-- video -->
        <div class="video-section">
            <div class="text-video">
                <h1 class="mask">Welcome to <br> velchala.com</h1>
            </div>          
            <div class="video-background"></div>
          
        </div>
        <!--/ video -->		
	
	</section>
    <!-- Hero section end -->
        
    <!-- great people-->
    <div class="great-people">
        <!-- Swiper -->
       
        <div class="swiper-container greatpeople-in">
            <div class="swiper-wrapper">
                <?php
                    for($i=0;$i<count($greatPeople);$i++){ ?>
                    <div class="swiper-slide"><img src="img/great/<?php echo $greatPeople[$i][0]?>" alt="<?php echo $greatPeople[$i][0]?>"></div>       
                <?php } ?>       
            </div>
        <!-- Add Pagination -->
        <!-- <div class="swiper-pagination"></div> -->
        </div>
    </div>
    <!--/ great people-->

    <!-- vsp and velchala -->
    <div class="vspsection">
        <!-- container fluid -->
        <div class="container-fluid px-0">
            <!-- row -->
            <div class="row no-gutters">
                <!-- col -->
                <div class="col-12 col-sm-12 col-md-6 vspcol vspleft wow animate__animated animate__fadeInUp">
                    <div class="img-div align-self-center">
                        <img src="img/velchalapng.png" alt="" class="img-fluid">
                    </div>
                    <div class="article-div align-self-center">
                        <h6 class="h6">Dr. Velchala Kondal rao</h6>
                        <h3>Personal Profile</h3>
                        <p class="pb-3">Dr. Rao is basically an educationist, known for his pioneering mind and promotinal drive and initiative. He has been the architect of many educational institutions in Andhra Pradesh. Left an indelible mark wherever he worked and whatever positions he held. The last and the last but one positions he held in the State of Andhra Pradesh were Director, telugu Academy and Joint Director, Higher Education.</p>
                        <a href="velchala.php" class="orange-btn">Read More</a>
                    </div>                   
                </div>
                <!--/ col -->
               <!-- col -->
               <div class="col-12 col-sm-12 col-md-6 vspcol vsprt wow animate__animated animate__fadeInDown">
               <div class="img-div">
                    <img src="img/vsp-png.png" alt="" class="img-fluid">
                </div>
                <div class="article-div align-self-center">
                    <h6 class="h6"> Viswanatha Sahitya Peetham</h6>
                    <h3>Founded by Velchala</h3>
                    <p class="pb-3">A literary cum cultual organization namely <b>"Vishwanatha Shitya Peetham"</b> is established by "<b>Sister Nivedita Foundation</b>"  at Red Hills, Hyderabad to function with effect from the month of july 2003. </p>
                    <a href="vsp.php" class="orange-btn">Read More</a>
                </div>
               
            </div>
            <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->
    </div>
    <!-- /vsp and velchala -->

    <!-- publications-->
    <div class="publications">
        <!-- custom container -->
        <div class="cust-container">
             <!-- title -->
             <div class="title-section wow animate__animated animate__fadeIn">
                <h4 class="h4">Publications</h4>
                <p>Written by Dr. Velchala Kondal Rao</p>               
            </div>
            <!--/ title -->
           
            <!-- books publications -->           
            <div class="swiper-container home-publications">
                <div class="swiper-wrapper ">
                    <!-- slide -->
                    <?php 
                    for($i=0;$i<count($homeBooks);$i++) {?>
                    <div class="swiper-slide">
                        <div class="img-box">
                            <img src="img/coverpages/<?php echo $homeBooks[$i][0]?>" alt="" class="img-fluid">
                            <!--hover -->
                            <div class="hover-section">                               
                                <a href="publication-detail.php"><span class="icon-search icomoon"></span></a>
                            </div>
                            <!--/ hover-->
                        </div>                        
                    </div>
                    <?php } ?>
                    <!--/ slide -->  
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
            <!-- /books publications -->
        </div>
        <!--/ custom container -->
        <p class="text-center pt-5">
            <a href="publications.php" class="orange-btn mx-auto wow animate__animated animate__slideInUp">View More</a>
       </p>
    </div>
    <!--/ publications -->

    <!-- gallery -->
    <div class="home-gallery">
        <!-- custom container -->
        <div class="cust-container gallery-block grid-gallery">
            <!-- title -->
            <div class="title-section  wow animate__animated animate__slideInUp">
                <h4 class="h4">Gallery</h4>
                <p>Images gallery by VElchala</p>               
            </div>
            <!--/ title -->

            <!-- gallery images -->
           <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- item -->
                    <?php 
                        for($i=0;$i<count($homeGallery);$i++){ ?>
                    <div class="col-6 col-sm-6 col-md-3 item  wow animate__animated animate__fadeInUp">
                        <a class="lightbox" href="img/gallery/<?php echo $homeGallery[$i][0]?>">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/<?php echo $homeGallery[$i][0]?>">
                        </a>
                    </div>
                        <?php } ?>
                    <!-- item -->                    
                </div>
                <!--/row -->               
           </div>
            <!--/ gallery images -->
           <p class="text-center pt-3">
                <a href="photo-albums.php" class="orange-btn mx-auto  wow animate__animated animate__fadeIn">View all Photo Gallery</a>
           </p>
        </div>
        <!--/ customcontainer -->        
    </div>
    <!--/ gallery -->

    <!-- poems -->
    <div class="home-poems">
        <!-- custome container -->
        <div class="cust-container">
            <h5 class="h5 wow animate__animated animate__slideInDown">Poems Written by Velchala</h5>

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center wow animate__animated animate__slideInUp">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <!-- swiper -->
                        <!-- Swiper -->
                            <div class="swiper-container poems-container">
                                <div class="swiper-wrapper">
                                    <?php 
                                        for($i=0;$i<count($homeTestimonials);$i++) { ?>
                                    <div class="swiper-slide">
                                        <article>
                                            <p>" <?php echo $homeTestimonials[$i][0]?>"</p>
                                        </article>
                                    </div>   
                                        <?php } ?>                                                    
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                        <!--/ swiper -->
                    </div>
                    <!-- col -->
                </div>
                <!--/row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ custom container -->
    </div>
    <!--/ poems -->
    
    <!--/ main-->
    <?php include 'includes/footer.php' ?>   

   <?php include 'includes/scripts.php' ?>
    <!-- hero slider -->
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/owl.carousel.min.js"></script> 
    <script src="js/jquery.nicescroll.min.js"></script> 
    <script src="js/main.js" ></script>  
</body>
</html>