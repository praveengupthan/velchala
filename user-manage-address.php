<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">
   <?php include 'includes/header-postlogin.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Manage Address</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="user-profile-information.php">Praveen Guptha</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Manage Address</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row py-3 userprofile-row">
                <!-- left col -->
                <div class="col-md-4 col-sm-4">
                    <?php include 'includes/usrleftnav.php' ?>
                </div>
                <!--/ left col -->
                <!-- right col -->
                <div class="col-md-8 col-sm-8">
                    <!-- right profile detail -->
                    <div class="user-profile-rt">

                      <div class="text-right">
                          <a data-toggle="modal" data-target="#newAddress" href="javascript:void(0)" class="orange-btn-border">+ Add Address</a>
                      </div>

                      <!-- address block -->
                      <div class="p-4 border my-3 address-block wow animate__animated animate__fadeInDown">
                            <p>
                              <span class="fsbold">Name:</span>
                              <span class="flight">Praveen Kumar Nandipati</span>
                            </p>
                            <p>
                              <span class="fsbold">Address:</span>
                              <span> lot No:915 Allwyn Colony, Phase 1, Kukatpally, Hyderabad, Hyderabad, 
    Telangana.</span>
                            </p>
                            <p>
                               <span class="fsbold">Email:</span>
                                <span>Praveennandipati@gmail.com</span>                             
                            </p>
                            <p>
                                <span class="fsbold">Phone:</span>
                                <span>+91 7995165789</span>                             
                            </p>
                            <p class="text-right">
                                <a href="javascript:void(0)" class=""><span class="icon-edit icomoon mr-1"></span>Edit</a>
                                <a href="javascript:void(0)" class="pl-3 delete-addressicon"><span class="icon-trash-o icomoon mr-1"></span>Delete</a>
                            </p>
                      </div>
                      <!--/ address block -->

                      
                      <!-- address block -->
                      <div class="p-4 border my-3 address-block wow animate__animated animate__fadeInDown">
                            <p>
                              <span class="fsbold">Name:</span>
                              <span class="flight">Praveen Guptha Nandipati</span>
                            </p>
                            <p>
                              <span class="fsbold">Address:</span>
                              <span> lot No:915 Allwyn Colony, Phase 1, Kukatpally, Hyderabad, Hyderabad, 
    Telangana.</span>
                            </p>
                            <p>
                               <span class="fsbold">Email:</span>
                                <span>Praveennandipati@gmail.com</span>                             
                            </p>
                            <p>
                                <span class="fsbold">Phone:</span>
                                <span>+91 7995165789</span>                             
                            </p>
                            <p class="text-right">
                                <a href="javascript:void(0)" class=""><span class="icon-edit icomoon mr-1"></span>Edit</a>
                                <a href="javascript:void(0)" class="pl-3 delete-addressicon"><span class="icon-trash-o icomoon mr-1"></span>Delete</a>
                            </p>
                      </div>
                      <!--/ address block -->

                    </div>
                    <!--/ right profile detail -->
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
       </div>
       <!-- /container -->   
                

       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
   <?php include 'includes/scripts.php' ?>    

   <!--image upload popup -->
   <!-- Modal -->
<div class="modal fade" id="newAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <!--form-->
      <form class="form">
          <!-- row -->
          <div class="row">
              <!-- col -->
              <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="firstName">First Name</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="firstName" aria-describedby="firstName" placeholder="First Name">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
              <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="secondName">Second Name</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="secondName" aria-describedby="firstName" placeholder="Second Name">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

              
               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="address01">Address Line 01</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="address01" aria-describedby="address01" placeholder="Address Line 01">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="address02">Address Line 02</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="address02" aria-describedby="address02" placeholder="Address Line 02">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="pinNumber">Pin Number</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="pinNumber" aria-describedby="pinNumber" placeholder="Write Pin Number">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

              <!-- col -->
              <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="landmark">Land Mark</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="landmark" aria-describedby="landmark" placeholder="Ex:Beside Hospital">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="city">City</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="city" aria-describedby="city" placeholder="Ex:Hyderabad">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

              
               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="State">State</label>
                        <div class="input-group">
                            <select id="State" class="form-control">
                                <option selected>Choose...</option>
                                <option>Telangana</option>
                                <option>Andhra Pradesh</option>
                                <option>Tamil Nadu</option>
                                <option>Karnataka</option>
                                <option>Kerala</option>
                                <option>Madhya Pradesh</option>
                            </select>
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

              
               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="mNumber">Mobile Number</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="mNumber" aria-describedby="mNumber" placeholder="Mobile Number">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="email">Email</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="email" aria-describedby="email" placeholder="Enter Valid Email Address">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

          </div>
          <!--/ row -->
      </form>
      <!--/ form -->
       
      </div>
      <div class="modal-footer">       
        <button type="button" class="orange-btn">Save changes</button>
      </div>
    </div>
  </div>
   
</div>
   <!--/image upload popup -->
   
    </body>
</html>