<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
    <?php include 'includes/arrayObjects.php'?>
</head>
<body class="animsition">

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Photo Gallery Albums</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>   
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Gallery</a></li>                    
                        <li class="breadcrumb-item active" aria-current="page"><span>Photo Gallery Albums</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-5">
                     <!-- col -->
                     <?php
                    for($i=0;$i<count($photoAlbums);$i++) {?>
                     <div class="col-sm-6 col-md-6 col-lg-4 wow animate__animated animate__fadeInUp">
                        <div class="book-item albumitem">
                            <figure class="bookcover">
                                <a href="photo-detail.php">
                                    <img src="img/albums/<?php echo $photoAlbums[$i][0]?>.jpg" alt="" class="img-fluid">
                                </a>                              
                                <span class="badge badge-pill photosnumber"><?php echo $photoAlbums[$i][1]?></span>
                            </figure>
                            <article>
                                <h2 class="h5">
                                    <a href="photo-detail.php"><?php echo $photoAlbums[$i][2]?></a>
                                </h2>
                                <div class="item-deails d-flex flex-wrap">
                                    <p class="small"><span class="icon-pin icomoon"></span><?php echo $photoAlbums[$i][3]?></p>
                                    <p class="small pl-4"><span class="icon-calendar icomoon pr-1"></span><?php echo $photoAlbums[$i][4]?></p>
                                </div> 
                            </article>
                        </div>
                    </div>
                    <?php } ?>
                    <!--/ col -->                 


                </div>
                <!--/ row -->
            </div>
            <!--/ container -->

       </div>
       <!--/ sub page body -->



    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>    
    </body>
</html>