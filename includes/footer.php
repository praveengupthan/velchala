
    <!-- footer -->
    <footer>
        <!-- container -->
        <div class="container">
            <!-- nav -->
            <ul class="nav justify-content-center aos-item" >
                <li class="nav-item">
                    <a href="index.php" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="velchala.php" class="nav-link">Velchala</a>
                </li>
                <li class="nav-item">
                    <a href="vsp.php" class="nav-link">VSP</a>
                </li>
                <li class="nav-item">
                    <a href="publications.php" class="nav-link">Publications</a>
                </li>
                <li class="nav-item">
                    <a href="blog-events.php" target="_blank" class="nav-link">Blog</a>
                </li>
                <li class="nav-item">
                    <a href="photo-albums.php" class="nav-link">Photo</a>
                </li>
                <li class="nav-item">
                    <a href="gallery-videos.php" class="nav-link">Videos</a>
                </li>                
                <li class="nav-item">
                    <a href="contact.php" class="nav-link">Contact</a>
                </li>
               
            </ul>
            <!--/ nav -->
            <p class="text-center"><i>© 2020 Velchala Kondal Rao. All rights reserved. </i></p>
            <ul class="nav justify-content-center termsnav aos-item">
                <li class="nav-item">
                    <a href="terms.php" class="nav-link">Terms of Services</a>
                </li>
                <li class="nav-item">
                    <a href="return-policy.php"  class="nav-link">Return Policy</a>
                </li>
                <li class="nav-item">
                    <a href="privacy.php"  class="nav-link">Privacy Policy</a>
                </li>
            </ul>
            <ul class="nav justify-content-center socialnav aos-item">
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-facebook icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-twitter icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-linkedin icomoon"></span></a>
                </li>
            </ul>            
        </div>
        <!--/container -->
        <a id="movetop" href="#" class="movetop"><span class="icon-arrow-up icomoon"></span></a>
    </footer>
    <!--/ footer -->