<link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
<!-- style sheets bootstrap and common styles -->
<link rel="stylesheet" href="css/bootstrap-material.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/icomoon.css">

<!-- file upload -->
<link rel="stylesheet" href="css/imageuploadify.min.css">

<!-- bootstrap nav -->
<link rel="stylesheet" href="css/bsnav.css">

<!-- style sheets grid gallery -->
<link rel="stylesheet" href="css/grid-gallery.css">
<link rel="stylesheet" href="css/baguetteBox.css">

<!-- style sheets for swipe and carousels -->
<link rel="stylesheet" href="css/swiper.min.css">   

<!-- responsive tab -->
<link rel="stylesheet" href="css/easy-responsive-tabs.css">

<!-- background video -->
<link rel="stylesheet" href="css/yt-video-background.css">











   