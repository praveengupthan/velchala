<!-- user left nav -->
<div class="user-leftNav whitebox">
    <h3 class="h5 text-uppercase fwhite bgorange p-2">Praveen Kumar N</h3>
    <ul>
        <li>
            <a href="user-profile-information.php">Profile</a>
        </li>
        <li>
            <a href="user-manage-address.php">Manage Address</a>
        </li>
        <li>
            <a href="user-orders.php">Orders History</a>
        </li>
        <li>
            <a href="user-wishlist.php">Wishlist Items</a>
        </li>
        <li>
            <a href="user-changepassword.php">Change Password</a>
        </li>
        <li>
            <a href="index.php">Logout</a>
        </li>
    </ul>
</div>
<!--/ user left nav -->