
 <!-- header -->
 <header class="fixed-top">
        <!-- gif animation slider -->
        <div class="gif-flag">
        <img src="img/indian-flag-waving-gif-animation-8.gif">
        </div>
    <!--/ gif animation slider -->        
       
    <!-- cust container -->
    <div class="container position-relative">
        <div class="navbar navbar-expand-lg bsnav px-0">
          <a class="navbar-brand" href="index.php">
            <img src="img/logo.svg" alt="" title="Velchala Kondal Rao">
          </a>
          <!-- <li class="nav-item search-icon"><a class="nav-link" id="searchIcon" href="#"><span class="icon-search icomoon"></span></a></li>   -->
            <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-md-end">
              <ul class="navbar-nav navbar-mobile mr-0">
                <li class="nav-item active"><a class="nav-link" href="indexT.php">సూచిక</a></li>
                <li class="nav-item"><a class="nav-link" href="velchalaT.php">వెల్చాల </a></li>
                <li class="nav-item"><a class="nav-link" href="vspT.php" data-toggle="tooltip" data-placement="top" title="విశ్వనాథ సాహిత్య పీఠం">VSP..</a></li>
                <li class="nav-item"><a class="nav-link" href="publications.php">ప్రచురణలు</a></li>              
                <li class="nav-item dropdown zoom"><a class="nav-link" href="#">జయంతి <i class="caret"></i></a>
                  <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="jayanthi.php">పత్రిక</a></li>
                    <li class="nav-item"><a class="nav-link" href="jayanthi-events.php">జయంతి ఈవెంట్స్</a></li>                                           
                  </ul>
                </li>
                <li class="nav-item dropdown zoom"><a class="nav-link" href="#">గ్యాలరీ <i class="caret"></i></a>
                  <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="gallery-poems.php">కవితలు</a></li>
                    <li class="nav-item"><a class="nav-link" href="photo-albums.php">ఫోటోలు</a></li>
                    <li class="nav-item"><a class="nav-link" href="gallery-videos.php">వీడియోలు</a></li>                        
                  </ul>
                </li>
                <li class="nav-item dropdown zoom"><a class="nav-link" href="#">బ్లాగ్ <i class="caret"></i></a>
                  <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="blog-events.php">సంఘటనలు</a></li>
                    <li class="nav-item"><a class="nav-link" href="blog-interviews.php">ఇంటర్వ్యూలు</a></li>
                    <li class="nav-item"><a class="nav-link" href="blog-news.php">వార్తలు</a></li>  
                    <li class="nav-item"><a class="nav-link" href="blog-articles.php">వ్యాసాలు</a></li>                      
                  </ul>
                </li>                 
                <li class="nav-item"><a class="nav-link t-link animate__animated animate__headShake animate__infinite" href="index.php"> <span>English</span></a></li>   
                <!-- <li class="nav-item">
                    <a class="nav-link cart-link" href="cart.php">
                        <span class="icon-shopping-cart icomoon"></span> 
                        <span class="value">10</span>
                    </a>
                </li> -->
                <!-- <li class="nav-item"><a class="nav-link" href="login.php"><span class="icon-user-o"></span> Login</a></li>   -->
              </ul>
            </div> 
          </div>   
          <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar"></div>
        </div>

          <!-- search section -->
          <div class="search-visible">
              <form class="d-flex justify-content-between m-0">
                  <div class="form-group">                                  
                      <input list="books" name="" id="" placeholder="Search Your Fvourite Book" class="form-control">
                      <datalist id="books">
                        <option value="Book Name will be here">
                        <option value="Book Name will be here">
                        <option value="Book Name will be here">
                        <option value="Book Name will be here">
                        <option value="Praveen">
                      </datalist>
                  
                  </div>
                  <div class="closeicon align-self-center">
                    <a href="javascript:void(0)" id="closeSearchIcon"><span class="icon-close icomoon"></span></a>
                  </div>
                 
              </form>
          </div>
          <!--/ search section -->

        </div>
        <!--/ cust container -->
    </header>
    <!--/ header -->


     