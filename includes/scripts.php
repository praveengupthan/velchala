<!-- scripts for jquery,  bootstrap and custom script files -->
<script src="js/jquery-3.2.1.min.js"></script>   
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/popper.js"></script>     
<!--  navigation -->     
<script src="js/bsnav.js"></script>   

<!-- file upload  -->
<script src="js/yt-video-background.min.js" charset="utf-8"></script>

<!-- typing effect -->
<script src="js/typingEffect.js"></script>
<!--[if lt IE 9]>
<script src="js/html5-shiv.js"></script>
<![end if ]-->
<script src="js/swiper.min.js"></script>

<!-- script files for grid gallery -->
<script src="js/baguetteBox.js"></script>    

<!-- responsive tab -->
<script src="js/easyResponsiveTabs.js"></script>

<!-- custom script -->
<script src="js/custom.js"></script>  

<!-- accordion -->
<script src="js/accordion.js"></script>

<!--/ stepper -->
<script src="js/stepper.js"></script>

<!-- file upload  -->
<script src="js/imageuploadify.min.js"></script>
