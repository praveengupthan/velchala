<?php
    //great people array objects
      $greatPeople=array(
        array("great01.png"),
        array("great02.png"),
        array("great03.png"),
        array("great04.png"),
        array("great05.png"),
        array("great06.png"),
        array("great07.png"),
        array("great08.png"),
        array("great09.png"),
        array("great10.png"),
        array("great11.png"),
        array("great12.png"),
        array("great13.png"),
        array("great14.png"),
        array("great15.png"),
        array("great16.png"),
        array("great17.png"),
        array("great18.png"),
    );

    //array objects home page books cover pages 
    $homeBooks=array(
        array("cover01.jpg"),
        array("cover02.jpg"),
        array("cover03.jpg"),
        array("cover04.jpg"),
        array("cover05.jpg"),
        array("cover06.jpg"),
        array("cover07.jpg"),
        array("cover08.jpg"),
        array("cover09.jpg"),
        array("cover10.jpg"),
        array("cover11.jpg"),
        array("cover12.jpg"),
        array("cover13.jpg"),
        array("cover14.jpg"),
        array("cover15.jpg"),
    );

    //array objedgs of home page gallery sample
    $homeGallery=array(
        array("gal01.JPG"),
        array("gal02.JPG"),
        array("gal03.JPG"),
        array("gal04.JPG"),
        array("gal05.JPG"),
        array("gal06.JPG"),
        array("gal07.JPG"),
        array("gal08.JPG"),
    );
    //array objects of home poems 
    $homeTestimonials=array(
        array("
        To be a 'man' is different than to be 'manly'.<br>
        One pertains to form, another to norm.<br>
        One to being, another to becoming.<br>
        One to entity, another to identity, Velchala!
        "),

        array("
        Educated have to set an example for the uneducated.<br>
        If the educated themselves can't be the role models<br>
        How then can the illiterates, the ignorants, the innocents<br>
        And the rest, Velchala!
        "),

        array("
        Education even if it is less is better<br>
        Than education which is more but poor.<br>
        Education even if it is started late is better<br>
        Than education which is started early but poorly, Velchala!<br>
        "),

        array("
        Every past has its glory as every present has, every future<br>
        Has good too, the bad, the mad too, the odd<br>
        As every person too has<br>
        Is and can be, Velchala!
        "),

        array("
        Tradition belongs to  infinity, eternity,<br>
        It is immesurable and unfathomable,<br>
        Its history had mattered then and matters too now<br>
        It is all that that had happened before, happening now, <br>
        and continues to happen ,Velchala!
        "),

        array("
        There is no modernity without tradition,<br>
        No tradition without modernity,<br>
        As there is no present without past, no future without present<br>
        no unity without diversity, no diversity without unity, Velchala!
        "),

        array("
        To respect and honour the wise and the nice,<br>
        The elders too, not for their size but for their rise,<br>
        So that, tomorrow when you too so grow<br>
        You may also be so respected and honoured, Velchala!
        "),

        array("
        Culture flows when not only lips greet<br>
        But when the hearts, the souls too beat to lovingly endearingly meet. <br>
        When each other to each other intensely float.<br>
        Culture pertains to one’s “Conscience”,<br>
        To heart’s stir, flutter,<br>
        To that, that touches you like a ripple.<br>
        Kisses you like a being a sublime, subtle.<br>
        When the cultured meets the cultured<br>
        Culture looks on calm and quiet<br>
        As if astonished at its own elevating<br>
        Ennobling charm<br>
        Like the artist looks at “Beauty” beyond the “Beautiful”<br>
        With a glint in his eye, a gleam in his vision.
        "),

        array("
        God, or call him by any other name.<br>
        But do not probe him by reason, rationale or logic,<br>
        But by faith, devotion, contemplation,<br>
        Meditation, concentration<br>
        By nothing <br>
        But by their magic and music.<br>
        God, would only be amenable to heart, spirit and soul<br>
        But not to the pure intellectual rationalistic rigmarole,<br>
        Not to an effort to find him by some argumentative investigative tool.<br>
        God, My Friend!<br>
        Would not reveal to you<br>
        If you are only intellectually smart<br>
        Strong only in your analytical dissecting art<br>
        But if only you are acutely, avowedly assiduously after him, like a devout<br>
        A simple, truthful, straightforward sort.
        "),

        array("
        I HAVE SEEN<br>
        I have seen cold cunning.<br>
        Unashamed avarice.<br>
        Unbridled arrogance.<br>
        Unlimited cant.<br>
        Unbelievable want.<br>
        Indescribable sham.<br>
        Infinite vanity.<br>
        Naked immorality.<br>
        Astonishing snobbery.<br>
        Appalling poverty.<br>
        Unthinkable sycophancy.<br>
        Unimaginable slavery.<br>
        Unwarranted envy and jealousy.<br>
        Why then should I ask<br>
        For this life’s longevity?<br>
        Bargain for its insufferable agony?
        "),
       
    );
    //array objects of home poems 
    $homePoemsTelugu=array(
        array("
        రోజా! <br>
        మేమెంతో  ఆశ్చర్యపోతాం నిను చూచి<br>
        నీవేల ఉండగలుగుతున్నావా అంట ప్రసన్నంగా<br>
        ఆ అనేకానేక వారాల మధ్యా పొరల మధ్యా<br>
        నీ చుట్టూరా కావలి కాస్తున్న ఆ ముళ్ల మధ్యా<br>
        ఎలా ఉండగలుగుతుంది నీ గర్భగుడి అంత ప్రసన్నంగా వాటిలో యని"),

        array("
        బ్రతుకుకర్థం తెలియాలంటే<br>
        బ్రతుకు లోతుల్లోకి తొంగి చూడు.<br>
        నీవు జీవించిన జీవితపు పుటలు,<br>
        నీకే కాస్త నిదానంగ నిలచి తిరిగేసి చూడు."),

        array("
        నేనెపుడూ ఆలోచిస్తాను ఆలోచించని వాళ్ల గురించి,<br>
        నేనాలోచించీ ఆలోచించీ అలసిపోతాను,<br>
        వారూ ఆలోచించక కూడా అలసినట్టగడపడతారు నాకు.<br>
        చాలానాళ్ళకు తెలిసింది నాకు ఆలోచించకపోతే కూడా<br>
        అలసిపోతారు మనుషులని.<br>
        అవును, 'కారు' మరీ వాడితే అలసిపోతుంది,<br>
        వాడకపోతే తుప్పు పడుతుంది,<br>
        మనిషి కూడా అంతే మరి!"
        ),

        array("
        తరచి చూస్తే నాకు అన్నీ మతాల్లాగే అనిపిస్తాయి.<br>
        మనుషుల్లో ఎవరి మతం వారిదిలాగే<br>
        సంఘాల్లో కూడా ఎవరి అభిమతాలు వారివే.<br>
        కొన్ని మతాల్లో దేవుని పూజ ఉంటుంది<br>
        కొన్ని మతాల్లో మనుషుల పూజ.<br>
        పూజలు అన్నికీ సమానమే, 'భజనలూ'<br>
        'ఆచారాలూ' 'హారాచారాలూ'<br>
        భక్తి పూజకు కూడా, భుక్తి పూజకు కూడా."
        ),

        array("
        విద్యావంతుడు విద్యలేని వాన్నెప్పుడూ దోపిడీ చేశాడు,<br>
        చరిత్ర చెబుతుంది, అనుభవమూ చెబుతుంది,<br>
        'సంపత్తి కాదు' విద్యే దోపిడికి అసలు కారణమని,<br>
        విద్య ఎంత ఎక్కువయితే అంత దోపిడీ ఎక్కువవుతుందని.<br>
        తరచి చూడండి మీకు చరిత్ర చెబుతుంది.<br>
        అలా అని విద్యక్కరలేదని కాదు అంటున్నది,<br>
        విద్య పేరట చలామణి అవుతున్నదంతా విద్య కాదని."
        ),

        array("
        ఎన్నాళ్ళయింది పూలు చూడక<br>
        చూడక చూడక ఇవాళ చూస్తే<br>
        అయ్యో! అదేమిో కాని<br>
        మనుషుల్లాగే కనపడుతున్నాయవి కూడా<br>
        వాి అందాలను, ఆనందాలను కోల్పోయి."
        ),

        array("
        వాన కురిసింది, వెలిసింది.<br>
        ఓ పుష్పంపై ఓ వాన బిందువు అలాగే ఆగిపోయింది<br>
        అల్లారు ముద్దుగా.<br>
        ఎంత అందంగా ఉందా బిందువు దాని అధరంపై!<br>
        అందమయిన వాకిటిలో  వేసిన అతి చక్కని ముగ్గులా,<br>
        అందాల రాణి చెక్కిలిలో చెమ్మగిల్లిన ఖిరిళీచీజిలి లా<br>
        సన్నని పొన్నని ముక్కు కొసన<br>
        అటూ ఇటూ ఆడుకుోంన్న ముక్కుపోగులా."
        ),

        array("
        ఆ నది ఉప్పొంగుతూ పరవళ్ళు త్రొక్కుతోంది<br>
        నా మది కూడా దానితో పాటు ఉరకలు వేసి<br>
        పొంగులా ఉప్పొంగుతోంది.<br>
        కొన్నాళ్ళకు ఆ నది ఉడిగి నడకగా నడుస్తూంది<br>
        కాని, నా మది, ఇంకా కొట్టుకొంటూనే ఉంటుంది.<br>
        నదికీ మదికీ అదే తేడా."
        ),

        array("
        నా భావాలెన్నో,<br>
        ఏ భాషలో వ్యక్తీకరించను నేను వాటిని?<br>
        భాషకేమి తెలుసు పాపం భావం బాధ?<br>
        బైటివాళ్ళకేం తెలుస్తాయి లోపలి వాళ్ళ బాధలు?<br>
        చెబితే తెలుస్తాయి కాని కొన్ని చెప్పలేనివి కూడా<br>
        ఉంటాయి కదా మరి!<br>
        కాలమే తీరుస్తుంది వాటి కష్టాలు<br>
        బాధ పడక్కరలేదు,<br>
        కాలమే కలానికీ, గళానికీ దాని భాష నందిస్తుంది."
        ),

        array("
        నాకు పూలంటే ఇష్టం,<br>
        పూలలాంటి  మనుషులంటే, మనుసులంటే ఇష్టం<br>
        అయినా పూలెవ్వరికిష్టముండవు<br>
        పూలలాంటి  మనుషులంటే, మనసులంటే, మమతలంటే<br>
        నా పిచ్చికాని,<br>
        ఒక్క పిచ్చివాళ్ళకు తప్ప?"
        ),


    );

    //array objects of publcations list 
    $publications=array(
        array(
            "cover01.jpg",
            "Viswanatha A Literary Legand",
            "250",
            "320",
            "pdf-book"
        ),
        array(
            "cover02.jpg",
            "Na Sahitya Vyasalu",
            "220",
            "320",
            "pdf-book"
        ),
        array(
            "cover03.jpg",
            "Ramayana Visha Vruksham",
            "210",
            "340",
            "pdf-book"
        ),
        array(
            "cover04.jpg",
            "Telangana Astitva Poratam",
            "250",
            "320",
            "pdf-book"
        ),
        array(
            "cover05.jpg",
            "కొన్ని కవితలు అనువాదాలు",
            "210",
            "260",
            "pdf-book"
        ),
        array(
            "cover06.jpg",
            "Jayanthi New Addition",
            "250",
            "320",
            "pdf-book"
        ),
        array(
            "cover07.jpg",
            "Geetanjali",
            "260",
            "300",
            "pdf-book"
        ),
        array(
            "cover08.jpg",
            "Mahanati Savitri",
            "300",
            "400",
            "pdf-book"
        ),
        array(
            "cover09.jpg",
            "Thousand Hoods",
            "250",
            "320",
            "pdf-book"
        ),
        array(
            "cover10.jpg",
            "Mahabharatam",
            "200",
            "220",
            "pdf-book"
        ),
        array(
            "cover11.jpg",
            "Wit & Wisdom of PV N Rao",
            "360",
            "320",
            "pdf-book"
        ),
        array(
            "cover12.jpg",
            "Chanikudi Neeti Bodhalu",
            "250",
            "320",
            "pdf-book"
        ),
        array(
            "cover13.jpg",
            "Kanyasulkam",
            "210",
            "400",
            "pdf-book"
        ),
        array(
            "cover14.jpg",
            "Asurudu, Parajitula Gadha",
            "250",
            "320",
            "pdf-book"
        ),
        array(
            "cover15.jpg",
            "Chanikya Neethi",
            "250",
            "320",
            "pdf-book"
        ),
        array(
            "cover01.jpg",
            "Viswanatha A Literary Legand",
            "230",
            "340",
            "pdf-book"
        ),
    );

    //array objects of jayanthi

    $jayanthiCover=array(
        array("1", "January 2020"),
        array("2", "February 2020"),
        array("3", "March 2020"),
        array("4", "April 2020"),
        array("5", "May 2020"),
        array("6", "June 2020"),
        array("7", "July 2020"),
        array("8", "August 2020"),
        array("9", "September 2020"),
        array("10", "October 2020"),
        array("11", "November 2020"),
        array("12", "December 2020"),
    );

    //array objects poems
    $galleryPoems=array(
        array("1"),
        array("2"),
        array("3"),
        array("4"),
        array("5"),
        array("6"),
        array("7"),
        array("8"),
        array("9"),
        array("10")       
    );

    //array photo albums 
    $photoAlbums=array(
        array("1","50 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("2","20 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("3","30 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("4","40 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("5","50 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("6","10 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("7","20 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("8","30 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("9","40 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("1","50 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("2","60 Photos","Event Name will be here","Hyderabad","22-11-2019"),
    );

    //array photo album detail
    $galleryPhotoDetail=array(
        array("1"),
        array("2"),
        array("3"),
        array("4"),
        array("5"),
        array("6"),
        array("7"),
        array("8"),
        array("9"),
        array("1"),
        array("2"),
        array("3"),
        array("4"),
        array("5"),
        array("6"),
        array("7"),
        array("8"),
        array("9")       
    );
    //array video gallery 
    $videoAlbums=array(
        array("https://www.youtube.com/embed/VNtVaV7U0BU","HMTV Special Focus On Sahitya Sangamam Rao",),
        array("https://www.youtube.com/embed/lLhadiHjiRE","HMTV Special Focus On Sahitya Sangamam  Part",),
        array("https://www.youtube.com/embed/5hpszhcgEyQ","HMTV Special Focus On Sahitya Sangamam Part",),
        array("https://www.youtube.com/embed/VNtVaV7U0BU","HMTV Special Focus On Sahitya Sangamam Rao",),
        array("https://www.youtube.com/embed/lLhadiHjiRE","HMTV Special Focus On Sahitya Sangamam  Part",),
        array("https://www.youtube.com/embed/5hpszhcgEyQ","HMTV Special Focus On Sahitya Sangamam Part",),
        array("https://www.youtube.com/embed/VNtVaV7U0BU","HMTV Special Focus On Sahitya Sangamam Rao",),
        array("https://www.youtube.com/embed/lLhadiHjiRE","HMTV Special Focus On Sahitya Sangamam  Part",),
        array("https://www.youtube.com/embed/5hpszhcgEyQ","HMTV Special Focus On Sahitya Sangamam Part",),
    );

    //array blog articles 
    $blogArticles=array(
        array(
            "The Republic Day",
            "Yet another year comes – A day, so called, a new day. It passes with a name. In a few moments of glitter and glee It mingles with the stillness, the shrillness of time",         
        ),
        array(
            "Rose",
            "Rose!
            You are a charm incomparable!
            I wonder, whether you are a flower
            Or a panorama of colours,
            A pageantry of smiles?",         
        ),       
    );

    //blog event array objects
    $blogEventItem=array(
        array(
            "1",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "25-06-2020"            
        ),
        array(
            "2",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Warangal",
            "21-08-2020"            
        ),
        array(
            "3",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "10-02-2020"            
        ),
        array(
            "4",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "15-04-2020"            
        ),
        array(
            "5",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "06-07-2020"            
        ),
        array(
            "6",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "16-06-2020"            
        ),
        array(
            "7",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Warangal",
            "25-06-2020"            
        ),
        array(
            "8",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Secunderabd",
            "25-06-2020"            
        ),
        array(
            "9",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "25-06-2020"            
        )
    );

?>