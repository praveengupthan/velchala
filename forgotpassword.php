<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

    <div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">
                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="index.php" class="brand-login">
                                <img src="img/logo.svg" alt="">
                            </a>
                            <h1 class="text-center flight pb-0">Forgot Password</h1>
                            <p class="text-center">We will send the link to Registered Email to Reset Your password </p>
                        </div>
                        <!-- form -->
                        <form class="form py-3">
                            <div class="form-group">
                                <label for="RegisteredEmail">Enter your Registered Email</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="RegisteredEmail" placeholder="Email">
                                </div>
                            </div>                                                       
                            <input type="submit" class="btn orange-btn w-100 mt-2" value="Sign in">
                            <p class="text-center">
                                Remember your Password? <a class="forange" href="login.php">Signin</a>
                            </p>
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ login section -->
                </div>
                <!--/ col -->               
            </div>
            <!--/row -->
        </div>
        <!--/ container fluid -->
    </div>

  
   
   
    <?php include 'includes/scripts.php' ?>    
    </body>
</html>