<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="animsition">

   <?php include 'includes/header.php' ?>
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Faq's</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Faq's</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!--r ow -->
                <div class="row justify-content-center pt-4">
                    <!-- col-->
                    <div class="col-md-8">

                        <div class="accordion">
                            <h3 class="panel-title">How can I check my order status?</h3>
                            <div class="panel-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique ex massa, non interdum nisl hendrerit nec. Sed metus dui, vehicula blandit nisi dictum, euismod bibendum lorem. Mauris vel ligula ut ligula facilisis porttitor quis sit amet leo. Donec sapien tellus, pulvinar a bibendum eu, ultrices vel risus. Nunc fermentum justo vitae lectus molestie, nec suscipit arcu tempor.</p>
                            </div>
                            <h3 class="panel-title">How do I know my order is confirmed?</h3>
                            <div class="panel-content">
                                <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                            </div>

                            <h3 class="panel-title">What are the different statuses in My Orders?</h3>
                            <div class="panel-content">
                                <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                            </div>

                            <h3 class="panel-title">Can I change my delivery address after I place an order?</h3>
                            <div class="panel-content">
                                <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                            </div>

                            <h3 class="panel-title">Can I open and check the contents of my package before accepting delivery?</h3>
                            <div class="panel-content">
                                <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                            </div>

                            <h3 class="panel-title">Why is the status on the courier's website different from My Orders?</h3>
                            <div class="panel-content">
                                <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                            </div>

                            <h3 class="panel-title">How do I know my order is confirmed?</h3>
                            <div class="panel-content">
                                <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                            </div>

                            <h3 class="panel-title">How do I know my order is confirmed?</h3>
                            <div class="panel-content">
                                <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                            </div>
                        </div>
                    </div>
                    <!--/col-->
                </div>
                <!--/row -->
            </div>
            <!--/ container -->
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?> 
    </body>
</html>