<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php' ?>
</head>
<body class="velchala-page animsition"> 

   <?php include 'includes/headerTelugu.php' ?>
    <!--main-->   
    <main class="subpage-main">

        <!-- velchala header -->
        <div class="velchalaHeader">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-6 col-12">
                        <article class="text-center">
                            <h1>కొండల రావు వెల్చాల   <span id="typed3"></span></h1>
                        </article>
                    </div>
                    <!-- /col -->
                     <!-- col -->
                     <div class="col-md-6 col-12 align-self-center rtpic-col">
                         <figure  class="velchala-fig">
                            <img src="img/velchala-primaryimg.jpg" alt="">
                         </figure>
                           <div id="background" class="clip"></div>
                            <div id="clip3" class="clip"></div>
                            <div id="clip4" class="clip"></div>
                            <div id="clipa" class="clip"></div>
                            <div id="clip1" class="clip"></div>
                            <div id="clip2" class="clip"></div>
                            <div id="clipb" class="clip"></div>
                        </div>
                   
                    <!-- /col -->
                </div>
                <!-- /row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ velchala header -->

        <!-- sub page body -->
        <section class="subpage-body velchalaspage">
            <!-- container -->
            <div class="container">
                <!-- single page -->
                <div class="singleScroll">
                <nav>
                    <ul>
                        <li><a href="#section-1">వెల్చాల బయోడేటా </a></li>
                        <li><a href="#section-2">వచనాలు</a></li>
                        <li><a href="#section-3">కవితలు</a></li>
                        <li><a href="#section-4">అనువాదాలు </a></li>
                        <li><a href="#section-5">సంకలనాలు</a></li>
                        <li><a href="#section-6">పురస్కారాలు</a></li>
                        
                    </ul>
                </nav>
                <!-- single page body -->
                <div class="singleBody">
                    <!-- section 1-->
                    <section id="section-1" class="wow animate__animated animate__fadeInUp">
                        <h2>వెల్చాల బయోడేటా  </h2>                        
                        <ul class="list-items">
                            <li>తెలుగు అకాడెమీ మాజీ సంచాలకులు</li>
                            <li>'జయంతి' సాహిత్య సాంస్కృతిక త్రైమాసిక పత్రిక సంపాదకులు</li>
                            <li>'బిజినెస్ విషన్' మేనేజ్మెంట్ విద్యా విషయక త్రైమాసిక పత్రిక సంపాదకులు</li>
                            <li>పోయెట్రీ సొసైటీ అఫ్ ఇండియా జీవిత సభ్యులు, పోయెట్రీ సొసైటీ అఫ్ హైదరాబాద్ సభ్యులు</li>
                            <li>ఇండియన్ ఇన్స్టిట్యూట్ అఫ్ పబ్లిక్ అడ్మినిస్ట్రేషన్  పర్మనెంట్ సభ్యులు</li>
                            <li>ఫోరమ్ ఫర్ హయ్యర్ ఎడ్యుకేషన్, హైదరాబాద్ ఉపాధ్యక్షులు</li>
                            <li>తెలుగు,ఇంగ్లీష్, ఉర్దూ భాషల్లో రచయిత, అనువాదకుడు, సంకలనకర్త.</li>
                        </ul>                       
                    </section>
                    <!--/ section 1-->
                    <!-- section 2-->
                    <section id="section-2" class="wow animate__animated animate__fadeInUp">
                        <h2>వచనాలు</h2>
                       <ul class="list-items d-block">
                         <li>భాషా-సాహిత్య-సాంస్కృతిక వ్యాసాలు</li>
                         <li>విద్యా విషయక వ్యాసాలు</li>
                         <li>ఆర్ధిక, సాంఘిక, రాజకీయ వ్యాసాలు</li>
                         <li>భావాలు-బంగారాలు</li>
                         <li>Wit & Wisdom</li>
                         <li>Ideas Are Gold</li>
                         <li>Gleanings and Rambling Thoughts</li>
                       </ul>
                    </section>
                    <!--/ section 2-->

                    <!-- section 3-->
                    <section id="section-3" class="wow animate__animated animate__fadeInUp">
                        <h2>కవితలు</h2>                      
                        <ul class="list-items">
                         <li>నా దేశం</li>
                         <li>విశ్వాత్మ</li>
                         <li>కావ్యం</li>
                         <li>ఎఱ్ఱగన్నేర్లు</li>
                         <li>ఒక పూవు పూచింది</li>
                         <li>పాపం గాంధీ</li>
                         <li>మరి రారేమీ ఇటు వెరుగు</li>
                         <li>అన్వేషణ</li>
                         <li>I Feel</li>
                         <li>I Remember</li>
                         <li>Poems to Note</li>
                         <li>Poems to Note, Quote and Float</li>
                         <li>18 Greates of India</li>
                         <li>The Essence of the Essence</li>
                       </ul>
                    </section>
                    <!-- /section 3-->

                    <!-- section 4-->
                    <section id="section-4" class="wow animate__animated animate__fadeInUp">
                        <h2>అనువాదాలు </h2>                        
                        <ul class="list-items">
                            <li>జంటలు-1 (గాలిబ్ కవితలు)</li>
                            <li>కొమ్మడుగు (తాహెరాబాను కవితలు)</li>
                            <li>టాగోర్ గీతాంజలి</li>
                            <li>ఊదా  అరుణ ఉదృత రేఖలు  (సంజీవదేవ్)</li>
                            <li>రిల్కే గారి రోజా</li>
                            <li>నిర్వచనాలు</li>
                            <li>అయితేనేమిలే</li>
                            <li>మైదానంలో మరీచిక (గ్రిగ్లని కవితలు)</li>
                            <li>Poetic Pattabhic (పట్టాభి రాగాల డజన్)</li>
                            <li>The Night The Nector Rained (తిలక్ - అమృతం కురిసిన రాత్రి)</li>
                        </ul>
                    </section>
                    <!-- /section 4-->

                    <!-- section 5-->
                    <section id="section-5" class="wow animate__animated animate__fadeInUp">
                        <h2>సంకలనాలు</h2>
                        <ul class="list-items">
                            <li> ఆస్కార్ వైల్డ్ చమత్కారాలు - చతురోక్తులు (Bold&Brilliant)</li>
                            <li>సాహిత్యధార</li>
                            <li>విశ్వనాథ వారి మురిపాల ముచ్చ్చట్లు</li>
                            <li> శేషేంద్ర - ఎ పొయెటిక్ లెజెండ్</li>
                            <li> విశ్వనాథ వారి ముద్దువద్దంలు (5)</li>
                            <li>కోవెల సంపత్కుమారాచార్య 'జయంతి' ప్రత్యేక సంచిక</li>
                            <li>తెలంగాణ అస్తిత్వ పోరాటం</li>
                            <li> సామల సదాశివ పై 'జయంతి' ప్రత్యేక సంచిక\</li>
                            <li>Viswanatha A Literary Legend</li>
                            <li>Telangaana Cultural Khazana (Coffee Table Book)</li>
                            <li>Thousand Hoods (Translation of Telugu "Veyi Padagalu")</li>
                            <li>మహానటి సావిత్రి</li>
                        </ul>
                        
                    </section>
                    <!-- /section 5-->

                    <!-- section 6-->
                    <section id="section-6" class="wow animate__animated animate__fadeInUp">
                        <h2>పురస్కారాలు</h2>
                        <ul class="list-items">
                            <li>పొట్టి శ్రీరాములు తెలుగు విశ్వవిద్యాలయం , హైదరాబాద్ నుండి గౌరవ డాక్టరేట్</li>
                            <li>పొట్టి శ్రీరాములు తెలుగు విశ్వవిద్యాలయం , హైదరాబాద్ నుండి ఉత్తమ అనువాద పురస్కారం</li>
                            <li>థాయిలాండ్, టర్కీ అంతర్జాతీయ కవుల సమ్మెనాలలో పాల్గొని గౌరవ పురస్కారం</li>
                            <li>మైఖేల్ మధుసూదన్ దత్  అవార్డ్ - కలకత్తా</li>
                            <li>సద్గురు శ్రీ శివానంద మూర్తి గారి "సనాతన ధర్మ చారిటబుల్ ట్రస్ట్" నుండి "ప్రతిభ పురస్కారం" అవార్డు</li>
                           
                        </ul>
                    </section>
                    <!-- /section 6-->

                  
                   
                </div>
                <!--/ single page body -->
                </div>
                <!--/ single scroll ends -->
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <?php include 'includes/footerTelugu.php' ?>
    

   <?php include 'includes/scripts.php' ?>  

</body>
</html>